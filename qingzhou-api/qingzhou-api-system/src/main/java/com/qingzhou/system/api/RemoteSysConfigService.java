package com.qingzhou.system.api;

import com.qingzhou.common.core.constants.HttpConstant;
import com.qingzhou.common.core.constants.ServiceConstant;
import com.qingzhou.common.core.web.nami.INamiClientFilter;
import org.noear.nami.annotation.NamiClient;
import org.noear.nami.annotation.NamiMapping;
import org.noear.solon.core.handle.Result;

/**
 * 远程调用系统服务 - 系统参数
 * @author xm
 */
@NamiClient(name = ServiceConstant.SERVICE_SYSTEM, path = ServiceConstant.PATH_TO_SYSTEM_CONFIG)
public interface RemoteSysConfigService extends INamiClientFilter {

    /**
     * 根据参数键名查询参数值
     * @param configKey
     */
    @NamiMapping(HttpConstant.HTTP_GET)
    Result<String> getSysConfigByKey(String configKey);

}
