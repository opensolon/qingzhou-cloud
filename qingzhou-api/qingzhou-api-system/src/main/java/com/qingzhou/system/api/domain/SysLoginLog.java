package com.qingzhou.system.api.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import com.qingzhou.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 登录日志 对象
 * @author ruoyi
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(value = "sys_login_log")
public class SysLoginLog extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "序号")
    @Id
    private Long loginId;

    /** 用户账号 */
    @Excel(name = "用户账号")
    private String userName;

    /** 地址 */
    @Excel(name = "地址")
    private String ipaddr;

    /** 登录设备类型 */
    private String deviceType;

    /** 状态（0成功 1失败） */
    @Excel(name = "状态", replace = { "成功_0", "失败_1" })
    private String status;

    @Column(ignore = true)
    private Date startTime;

    @Column(ignore = true)
    private Date endTime;

}
