package com.qingzhou.system.api;

import com.qingzhou.common.core.constants.HttpConstant;
import com.qingzhou.common.core.constants.ServiceConstant;
import com.qingzhou.common.core.web.nami.INamiClientFilter;
import com.qingzhou.system.api.domain.SysLoginLog;
import com.qingzhou.system.api.domain.SysOperLog;
import org.noear.nami.annotation.NamiBody;
import org.noear.nami.annotation.NamiClient;
import org.noear.nami.annotation.NamiMapping;
import org.noear.solon.core.handle.Result;

/**
 * 远程调用系统服务 - 日志
 * @author xm
 */
@NamiClient(name = ServiceConstant.SERVICE_SYSTEM, path = ServiceConstant.PATH_TO_SYSTEM_LOG)
public interface RemoteSysLogService extends INamiClientFilter {

    /**
     * 记录登录日志
     * @param sysLoginLog
     */
    @NamiMapping(HttpConstant.HTTP_POST)
    Result<Void> saveLoginLog(@NamiBody SysLoginLog sysLoginLog);

    /**
     * 记录操作日志
     * @param sysOperLog
     */
    @NamiMapping(HttpConstant.HTTP_POST)
    Result<Void> saveOperLog(@NamiBody SysOperLog sysOperLog);

}
