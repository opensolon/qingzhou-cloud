## 轻舟文档中心
基于 VuePress 编写 Markdown(也就是.md文件) 来生成在线文档

# VuePress官网
https://v1.vuepress.vuejs.org/zh


### 运行指南
```bash
# 进入项目目录
cd qingzhou-docs

# 安装依赖
npm install

# 启动服务
npm run dev
```

### 开发指南
1、在 /docs 目录下创建文件夹 a ，并在 a 中创建文件 b.md\
2、在 /docs/.vuepress/sidebar.js 中增加侧边栏配置

```javascript
// 侧边栏
module.exports = [
  {
    title: '文档a',
    sidebarDepth: 0,  // 可选的, 默认值是 1
    children: [ '/a/b' ]
  }
]
```

### 打包指南
```bash
# 构建生产环境
npm run build
```


# Markdown在线编写地址
https://markdown.com.cn/editor/
