
module.exports = {
  // 部署站点的基础路径
  base: '/',
  title: 'QingZhou',
  // 注入到当前页面的 HTML <head> 中的标签
  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }], // 网页标签的图标
  ],
  host: '0.0.0.0',
  port: 80,
  devServer: {
    useLocalIp: true, // 避免打开浏览器为0.0.0.0，需手动改IP的情况
    open: true,  // 自动打开浏览器
  },
  // 指定 vuepress build 的输出目录
  dest: 'dist',
  // 多语言支持
  locales: {
    '/': {
      lang: 'zh-CN'
    }
  },
  // 你可以监听任何想监听的文件，文件变动将会触发 vuepress 重新构建，并实时更新。
  extraWatchFiles: [
    '.vuepress/nav.js',
    '.vuepress/sidebar.js'
  ],
  // 主题配置
  themeConfig: {
    logo: '/favicon.ico', // 导航栏左侧logo
    lastUpdated: '最后更新时间', // 文档更新时间：每个文件git最后提交的时间
    // 右上角导航栏
    nav: require("./nav.js"),
    // 侧边栏
    sidebar: require("./sidebar.js")
  },
  // 插件
  plugins: [

  ]
}
