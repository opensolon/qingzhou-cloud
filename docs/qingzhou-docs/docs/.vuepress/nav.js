// 右上角导航栏
module.exports = [
  {
    text: '首页',
    link: '/'  // 内部链接，以docs为根目录
  },
  {
    text: '概述',
    link: '/guide/adumbrate/'  // 内部链接（以 / 开头并结尾，默认读取文件夹下的 README.md）
  },
  {
    text: '单体版',
    link: '/guide/boot/adumbrate'  // 内部链接（以 / 开头，不写 .md 后缀，默认读取文件夹下对应的 .md 文件）
  },
  {
    text: '微服务版',
    link: '/guide/cloud/adumbrate'
  },
  {
    text: '移动端版',
    link: '/guide/app/adumbrate'
  },
  {
    text: 'Gitee',
    // 下拉列表
    items: [
      { text: '单体版', link: 'https://gitee.com/opensolon/qingzhou-boot' },
      { text: '微服务版', link: 'https://gitee.com/opensolon/qingzhou-cloud' },
      { text: '移动端版', link: '/' }
    ]
  }
]
