---
title: 项目部署
prev: false
# next: ./v2
---

## 后端
### 1、打包
* Maven 版
  ```bash
  mvn clean package
  ```

  ::: tip 默认打 jar 包
  打包后会在每个微服务模块的 target 目录下生成 jar 包文件 qingzhou-*.jar
  :::

* Gradle 版\
  双击：`Gradle插件/qingzhou-cloud/Tasks/build`下的`clean`和`build`。
  ::: tip 默认打 jar 包
  打包后会在每个微服务模块的 build/libs 目录下生成 jar 包文件 qingzhou-*.jar
  :::

### 2、运行
```bash
java –jar qingzhou-*.jar

# 按需运行 jar 包，用到哪个运行哪个
```

::: tip 支持添加启动参数
```bash
java –jar qingzhou-admin.jar --server.port=8081

java –jar qingzhou-admin.jar --solon.env=dev

java –jar qingzhou-admin.jar --nacos.namespace=ceb00317-a8b0-4a9a-a695-2274b7b53b5a
```
详见 [Solon 配置](https://solon.noear.org/article/482)
:::

### 3、jar包加密
如果您不希望 jar 包被反编译导致泄露源码，您可以对 jar 包进行加密（目前仅支持`Maven`版）。\
加密工具：[ClassFinal](https://gitee.com/lcm742320521/class-final)\
加密只需一步：在每个需要打包的微服务模块添加加密插件（`qingzhou-auth/pom.xml`、`qingzhou-modules/qingzhou-modules-system/pom.xml`等）
```xml
<!-- pom.xml -->
<build>
  <finalName>${project.artifactId}</finalName>
  <plugins>
    <plugin>
      <groupId>org.noear</groupId>
      <artifactId>solon-maven-plugin</artifactId>
    </plugin>

    <!-- jar包加密 -->
    <plugin>
      <groupId>com.gitee.lcm742320521</groupId>
      <artifactId>classfinal-maven-plugin</artifactId>
      <version>1.4.0</version>
      <configuration>
        <libjars>qingzhou-*.jar</libjars><!-- jar包lib下要加密jar文件名(可为空,多个用","分割) -->
        <packages>com.qingzhou</packages><!-- 加密的包名(可为空,多个用","分割。应包含所有需要加密的类的包名) -->
        <cfgfiles>*.yml</cfgfiles><!-- 需要加密的配置文件，一般是resources目录下的yml或properties文件(可为空,多个用","分割) -->
        <password>#</password><!-- 加密密码，如果是#号，则使用无密码模式加密 -->
      </configuration>
      <executions>
        <execution>
          <phase>package</phase>
          <goals>
            <goal>classFinal</goal>
          </goals>
        </execution>
      </executions>
    </plugin>
  </plugins>
</build>
```
添加以上打包加密插件后，正常打包即可（打包方式不变）。打包成功后可在每个微服务模块的 target 目录下看到两个 jar 包：
  * qingzhou-*.jar
  * qingzhou-*-encrypted.jar *加密后的*

请运行加密后的 jar 包，运行命令为：
```bash
java -javaagent:qingzhou-*-encrypted.jar -jar qingzhou-*-encrypted.jar
```


## 前端
### 1、打包
```bash
npm run build:prod
```

::: tip 提示
打包成功之后，会在根目录生成 dist 文件夹，通常是 .js 、.css、index.html 等静态文件。\
将 dist 文件夹发布到你的 Nginx 或者静态服务器即可，其中的 index.html 是后台服务的入口页面。
:::

### 2、Nginx 配置参考（v1.x）
~~~
worker_processes  1;

events {
  worker_connections  1024;
}

http {
  include            mime.types;
  default_type       application/octet-stream;
  sendfile           on;
  keepalive_timeout  65;

  server {
    listen       80;
    server_name  localhost;
    charset      utf-8;

    location / {
      root  /home/qingzhou/ui;
      try_files $uri $uri/ /index.html;
      index  index.html index.htm;
    }

    location /prod-api/ {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header REMOTE-HOST $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass http://localhost:8080/;
    }

    error_page  500 502 503 504  /50x.html;
    location = /50x.html {
      root  html;
    }
  }
}
~~~

### 3、Nginx 配置参考（v2.x）
::: warning 注意
`v2.x`版本使用 Nginx 进行请求分发。

配置文件：qingzhou-cloud/sql/nginx.conf
:::

~~~
worker_processes  1;

events {
  worker_connections  1024;
}

http {
  include            mime.types;
  default_type       application/octet-stream;
  sendfile           on;
  keepalive_timeout  65;

  server {
    listen       80;
    server_name  localhost;
    charset      utf-8;

    location / {
      root  /home/qingzhou/ui;
      try_files $uri $uri/ /index.html;
      index  index.html index.htm;
    }

    location /prod-api/auth/ {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header REMOTE-HOST $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass http://qingzhou-auth/;
    }

    location /prod-api/system/ {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header REMOTE-HOST $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass http://qingzhou-system/;
    }

    location /prod-api/file/ {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header REMOTE-HOST $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass http://qingzhou-file/;
    }

    location /prod-api/gen/ {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header REMOTE-HOST $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass http://qingzhou-gen/;
    }

    location /prod-api/schedule/ {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header REMOTE-HOST $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass http://qingzhou-schedule/;
    }

    location /prod-api/demo/ {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header REMOTE-HOST $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass http://qingzhou-demo/;
    }

    error_page  500 502 503 504  /50x.html;
    location = /50x.html {
      root  html;
    }
  }

  upstream qingzhou-auth {
    server localhost:8101;
  }

  upstream qingzhou-system {
    server localhost:8102;
  }

  upstream qingzhou-file {
    server localhost:8091;
  }

  upstream qingzhou-gen {
    server localhost:8092;
  }

  upstream qingzhou-schedule {
    server localhost:8093;
  }

  upstream qingzhou-demo {
    server localhost:8103;
  }

}
~~~
