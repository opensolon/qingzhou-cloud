---
title: 后端手册
prev: false
# next: ./ops
sidebarDepth: 2
---

::: danger 注意
请牢记您是在写 Solon 项目，而不是 SpringBoot 项目。
:::
::: warning 注意
微服务版和单体版的很多功能都是一致的。这里只列举出有区别的功能说明。未列举的功能请前往 [单体版文档](../boot/dev.md)。
:::

## 演示模块
`qingzhou-modules-demo`是演示模块。如果您想创建新的服务模块，请参照此演示模块进行创建，您也可以直接改造演示模块。

## 配置自动刷新
所有服务模块的配置文件都在`Nacos`中，以下配置支持自动刷新（autoRefreshed = true）：
* 验证码配置：qingzhou.auth.*
* 文件配置：qingzhou.file.*
* 代码生成配置：qingzhou.gen.*
::: warning 注意
由于 Solon 内部机制的设计，如果您在`Nacos`中配置了数组类型的配置，然后在后端使用数组或List接收，并开启配置自动刷新，当配置变化后，会导致接收的配置参数重复导致混乱。

因此如果您想实现数组类型的配置自动刷新，请配置为字符串形式，并在后端使用字符串切割为数组（参考文件配置或代码生成配置的自动刷新）。
:::

## 上传下载
* 上传
  * 前端采用`ElementUI`的上传组件 Upload。
  * 后端支持本地文件上传及云存储（`qingzhou-modules-file`模块）。
  ::: warning 注意
  * 文件上传针对文件类型和文件大小在前端和后端都做了限制。
    * 后端文件上传配置示例
    ```yaml
    qingzhou:
      file:
        # 使用的文件服务类型: s3 本地文件存储、aliyun 阿里云对象存储、qiniu 七牛云对象存储、minio
        # 请前往 file 模块的 pom.xml 注释掉不用的文件服务依赖
        type: s3
        # 允许上传的文件大小（单位：MB，设为-1不限制）
        maxSize: 2
        # 允许上传的文件类型
        fileType: jpg, jpeg, png, doc, docx, xls, xlsx, ppt, txt, pdf, zip

    solon:
      cloud:
        # 本地文件 
        file:
          s3:
            file:
              default: local_bucket
              buckets:
                local_bucket:
                  endpoint: C:\Users\xiaoming\Desktop
                  requestPrefix: http://localhost:90/qingzhou-file
                # more_bucket:
                #   regionId: cn-east-1
                #   endpoint: http://s3.ladydaily.com
                #   accessKey: iWeU7cOoPLRokg2Hdat0jGQC
                #   secretKey: ZZIH6mT4VLAy68mVP80F7LiB5SpSEM7N
        # 阿里云 OSS
        aliyun:
          oss:
            file:
              endpoint: oss-cn-xxx.aliyuncs.com
              bucket: world-data-dev
              accessKey: iWeU7cOoPLRokg2Hdat0jGQC
              secretKey: ZZIH6mT4VLAy68mVP80F7LiB5SpSEM7N
        # 七牛云 OSS
        qiniu:
          kodo:
            file:
              regionId: cn-east-1
              endpoint: https://xxx.yyy.zzz
              requestPrefix: https://aaa.bbb.ccc
              bucket: world-data-dev
              accessKey: iWeU7cOoPLRokg2Hdat0jGQC
              secretKey: ZZIH6mT4VLAy68mVP80F7LiB5SpSEM7N
        # minio
        minio:
          file:
            regionId: cn-east-1
            endpoint: https://play.min.io
            bucket: asiatrip
            accessKey: iWeU7cOoPLRokg2Hdat0jGQC
            secretKey: ZZIH6mT4VLAy68mVP80F7LiB5SpSEM7N
    ```
    * 默认使用本地文书上传。如果您想更换为其他上传方式（比如阿里云OSS），请先前往`qingzhou-modules-file`模块的`pom.xml`文件或`build.gradle`文件，打开阿里云OSS的注释，并注释掉其它的；并且修改上方文件上传配置：`qingzhou.file.type: aliyun`，重启`qingzhou-modules-file`服务。
    ```xml
    <!-- Maven 版 -->
    <dependencies>
      <!-- 本地文件 -->
      <!--<dependency>-->
        <!--<groupId>org.noear</groupId>-->
        <!--<artifactId>file-s3-solon-cloud-plugin</artifactId>-->
      <!--</dependency>-->

      <!-- 阿里云 OSS -->
      <dependency>
        <groupId>org.noear</groupId>
        <artifactId>aliyun-oss-solon-cloud-plugin</artifactId>
      </dependency>

      <!-- 七牛云 OSS -->
      <!--<dependency>-->
        <!--<groupId>org.noear</groupId>-->
        <!--<artifactId>qiniu-kodo-solon-cloud-plugin</artifactId>-->
      <!--</dependency>-->

      <!-- minio（minio基于 minio8 sdk，minio7基于 minio7 sdk） -->
      <!--<dependency>-->
        <!--<groupId>org.noear</groupId>-->
        <!--<artifactId>minio-solon-cloud-plugin</artifactId>-->
      <!--</dependency>-->
      <!--<dependency>-->
        <!--<groupId>org.noear</groupId>-->
        <!--<artifactId>minio7-solon-cloud-plugin</artifactId>-->
      <!--</dependency>-->
    </dependencies>
    ```
    ```gradle
    //  Gradle 版
    dependencies {
      ...
      //  implementation 'org.noear:file-s3-solon-cloud-plugin'
      implementation 'org.noear:aliyun-oss-solon-cloud-plugin'
      //  implementation 'org.noear:qiniu-kodo-solon-cloud-plugin'
      //  implementation 'org.noear:minio-solon-cloud-plugin'
      //  implementation 'org.noear:minio7-solon-cloud-plugin'
    }
    ```
  * 如果您使用的是本地文件上传，您需要搭配工具来实现本地文件的访问（例如 Nginx）。
    * Nginx配置示例
    ```
    server {
      listen       90;
      server_name  localhost;

      location /qingzhou-file {
        alias   C:/Users/xiaoming/Desktop/;
        autoindex on;
      }
    }
    ```
  :::
  * 文件上传大小受以下几处限制：
    * 前端上传组件
    * 后端Web服务器的请求体大小：server.request.maxFileSize: 2mb
    * 后端文件上传配置：qingzhou.file.maxSize: 2
    * 前端部署到服务器后（例如 Nginx），Nginx限制的请求体大小
* 下载\
数据导出即采用了文件下载，您可以参照数据导出的实现。

## 权限注解
与单体版的不同之处在于，微服务有单独的安全模块`qingzhou-common-security`。所有的微服务模块都应引入此安全模块。

## 统一返回结果 & 异常处理
同样的，微服务有单独的统一返回模块`qingzhou-common-result`。所有的微服务模块都应引入此统一返回模块。

## 熔断限流
使用`sentinel`实现。
* 配置示例（share-config-dev.yml）
```yaml
solon:
  cloud:
    local:
      # 限流
      breaker:
        # 根断路器的阀值（即默认阀值，必须大于0）
        root: 1000
        # global 为断路器名称（不配置或配置0，则取根断路器的阀值）
        # global: 100
```
* 全局限流：`com.qingzhou.common.security.filter.GlobalFilter`
* [Solon熔断限流](https://solon.noear.org/article/160)

## 系统接口文档
从`v2.5.0`版本开始增加了文档模块，使用`knife4j`实现。\
* 接口文档配置（位于 nacos 的 share-config-dev.yml 中）
  ```yaml
  knife4j:
    # 开启增强配置
    enable: true
    # 开启生产环境屏蔽
    production: false
    # 签权
    # basic:
    #   enable: true
    #   username: admin
    #   password: 123456
    # 前端UI的个性化配置
    setting:
      enableVersion: true
      enableOpenApi: false
      enableFooter: false
  ```
* 为服务模块引入文档模块
  ```xml
  <!-- 接口文档模块 -->
  <dependency>
      <groupId>com.qingzhou</groupId>
      <artifactId>qingzhou-common-swagger</artifactId>
  </dependency>
  ```
* 接口文档摘要：基于配置文件构建（在服务模块的配置文件中增加下方配置）
  ```yaml
  solon:
    # 接口文档
    docs:
      routes:
        - id: openapi  # 固定值
          # groupName: group-name
          info:
            title: 接口文档
            description: 在线API文档
  #          termsOfService: https://gitee.com/noear/solon
            contact:
              name: xm
            version: 1.0
          schemes:
            - HTTP
            - HTTPS
          globalResponseInData: true
          globalResult: org.noear.solon.core.handle.Result
          apis:
            - basePackage: com.qingzhou.system.controller
  ```
* 给接口添加注解
  ```java {1,6}
  @Api("系统参数配置")
  @Controller
  @Mapping("system/config")
  public class SysConfigController extends BaseController {

    @ApiOperation("列表查询")
    @Get
    @Mapping("list")
    public PageData list(Page<SysConfig> page, SysConfig sysConfig) {
      QueryWrapper qw = getQW(sysConfig);
      Page<SysConfig> result = sysConfigService.page(page, qw);
      return getPageData(result);
    }

  }
  ```
* 启动`QingZhouDocsApplication`，登录系统（系统工具 -> 系统接口），使用接口文档配置中的签权的账号密码登录
* 如果不需要某个服务模块出现在文档中，可以在`QingZhouDocsApplication`的配置文件中配置忽略该服务
  ```yaml{15-16}
  solon:
    # 接口文档
    docs:
      # 自动发现配置
      discover:
        # 是否启用自动发现
        enabled: true
        # 目标服务的文档接口路径模式（要么带变量 {service}，要么用统一固定值）
        uriPattern: swagger/v2?group=openapi
        # 同步目标服务上下线状态（如果下线，则文档不显示）
        syncStatus: false
        # 签权
        basicAuth:
          admin: 123456
        # 排除目标服务名
        excludedServices:
          - qingzhou-auth
          - qingzhou-docs
          - qingzhou-file
          - qingzhou-gen
          - qingzhou-schedule
          - qingzhou-monitor
  ```

## 系统监控
系统监控分为服务端和客户端，服务端有单独的模块`qingzhou-visual-monitor`，其它每个服务模块作为客户端。
* 您需要先启动`qingzhou-visual-monitor`服务模块
* 打开每个服务模块主启动类上的`@EnableAdminClient`注解的注释
* 监控配置的服务端配置在`qingzhou-monitor-dev.yml`中，客户端配置在`share-config-dev.yml`中。
* 登录系统（系统工具 -> Admin控制台），使用监控配置中的基础签权的账号密码登录
