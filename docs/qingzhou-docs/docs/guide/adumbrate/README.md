# 概述
<h1 align="center">QingZhou</h1>

<h4 align="center">基于 Solon、Vue 的权限后台管理系统</h4>

<p align="center">
  <a href="#">
    <img src="https://img.shields.io/badge/License-Apache 2.0-green" />
  </a>
</p>

### 轻舟是什么？
轻舟是一个后台管理系统（快速开发脚手架），属于 [若依](https://www.ruoyi.vip) 框架的 Solon 版本，具备与若依相同的系统功能。如果您有若依脚手架的开发经验，可轻松上手。

> 若依是 SpringBoot + Vue  
轻舟是 Solon + Vue  
前端是一样的，区别在后端。

### Solon 是什么？
为什么要写轻舟？因为 [Solon](https://solon.noear.org)。  
若依后端是基于 SpringBoot 开发的，而 Solon 与 SpringBoot 同属于 Java 语言开发框架。优势在于其构建的应用，打包体积更小，启动速度更快，内存占用更低，并发更高。

***看下对比：***
|    |  轻舟  |  若依  |
|  :--:  |  :--:  |  :--:  |
|  打包体积（MB）  |  46.64  |  72.15  |
|  启动速度（s）  |  1.4  |  4.6  |
|  内存占用（%）  |  4.1  |  8.1  |

> 参与过运维的同学都知道，SpringBoot 项目打包动辄几百兆，跑起来内存占用轻松达到 GB 级别。  
用了 Solon ，内存和存储预算直接减半，在云原生时代岂不是很香？

### 版本
目前提供了2个版本：
* 单体版：[QingZhou Boot](https://gitee.com/opensolon/qingzhou-boot)
* 微服务版：[QingZhou Cloud](https://gitee.com/opensolon/qingzhou-cloud)

### 在线体验
[https://qing-zhou.cn](https://qing-zhou.cn)\
账密：admin/Admin@123
