---
title: 前端手册
prev: false
# next: ./ops
---

## 说明
* 前端手册不一定全面，您也可以参考 [若依前端手册](https://doc.ruoyi.vip/ruoyi-vue/document/qdsc.html)。
* 先看一看`简介（单体版）`中的`项目结构`，帮助理解。
* `Vue2`版本和`Vue3`版本的前端，目录结构几乎一致（除了`Vue3`版本的`index.html`从`public`目录下移到了根目录）。
* `Vue2`版本的`store`使用的是`Vuex`，`Vue3`版本的`store`使用的是`Pinia`。
* `Vue2`版本使用的是`Element-UI`，`Vue3`版本使用的是`Element-Plus`。
* `Vue3`版本已经安装了`unplugin-auto-import`和`unplugin-vue-setup-extend-plus`，因此您可以在组件文件的`script`标签中定义组件名称`name`，例如`<script name="User" setup>`，
以及您在组件文件中无需手动导入`ref`、`computed`、`toRefs`、`defineProps`等等。

## 项目首页
项目运行后，输入账密登录后的首页，默认是项目简介。  
另外准备好了一个由echarts图表实现的统计页面，在`src/views/index_v1.vue`。  
将`src/views/index_v1.vue`和`src/views/index.vue`内容对换即可。

## 全局
为了使用方便，注册了一些全局的方法、组件、自定义指令等，您可以在`src/plugins/index.js`中看到。  
全局的方法、组件、自定义指令等，全局可以直接使用，无需再单独注册。
|  全局方法  |  功能  |
|  :--:  |  :--:  |
|  $parseTime  |  格式化时间  |
|  $download  |  下载文件  |
|  $modal  |  模态框对象  |
|  $tab  |  页签操作  |
|  $resetForm  |  表单重置  |
|  $selectDictLabel  |  回显数据字典  |
|  $handleTree  |  构造树型结构数据  |
> 全局方法统一使用`$`开头。  
`Vue2`中使用`this.$xxx`，`Vue3`中使用`proxy.$xxx`。

|  全局组件  |  功能  |
|  :--:  |  :--:  |
|  DictTag  |  字典标签组件  |
|  Editor  |  富文本组件  |
|  FileUpload  |  文件上传组件  |
|  ImagePreview  |  图片预览组件  |
|  ImageUpload  |  图片上传组件  |
|  selectDictLabel  |  分页组件  |
|  RightToolbar  |  自定义表格工具组件  |
> 直接在模板中使用`<xxx-xxx />`。  
推荐全小写 + 短横线的方式，比如：`<dict-tag />`

|  全局指令  |  功能  |
|  :--:  |  :--:  |
|  v-clipboard  |  复制文本  |
|  v-has-permi  |  字符权限  |
|  v-has-role  |  角色权限  |
|  v-dialog-drag  |  弹窗拖拽（Vue2版）  |
> 直接在元素标签中使用`<el-button v-has-permi="['system:user:remove']" />`。  
此时该按钮只能被拥有`system:user:remove`字符权限的用户看到并操作。

## ESlint
想使用`ESlint`，打开前端项目根目录下的`.eslintignore`，把配置的目录或通配符文件所在行注释掉即可。
``` {2,4,6,8,10}
# 忽略build目录下类型为js的文件的语法检查
# build/*.js
# 忽略src/assets目录下文件的语法检查
# src/assets
# 忽略public目录下文件的语法检查
# public
# 忽略当前目录下为js的文件的语法检查
# *.js
# 忽略当前目录下为vue的文件的语法检查
# *.vue
```
> `ESlint`相关配置在前端项目根目录下的`.eslintrc.js`。

## 组件
`src/components`目录下已提供了很多组件。  
包括但不限于：echarts图表、全屏等等。

## utils工具
`src/utils`目录下已提供了很多常用的方法。  
包括但不限于：缓存、浮点型运算、日期格式化、文件下载、非对称加解密、校验、防抖等等。
