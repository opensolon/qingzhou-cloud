package com.qingzhou.common.core.web.domain.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.ColumnMask;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.mask.Masks;
import com.qingzhou.common.core.constants.UserConstant;
import com.qingzhou.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.noear.solon.validation.annotation.Email;
import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.NotBlank;

import java.util.Date;
import java.util.List;

/**
 * 系统用户 对象
 * @author xm
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(value = "sys_user")
public class SysUser extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    @Excel(name = "用户序号")
    @Id
    private Long userId;

    /** 部门ID */
    private Long deptId;

    /** 账号 */
    @Excel(name = "账号")
    @NotBlank(message = "账号不能为空")
    @Length(min = UserConstant.USERNAME_MIN_LENGTH,
            max = UserConstant.USERNAME_MAX_LENGTH,
            message = "账号长度应在" + UserConstant.USERNAME_MIN_LENGTH + "和" + UserConstant.USERNAME_MAX_LENGTH + "之间")
    private String userName;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    @Length(max = 30, message = "用户昵称长度不能超过30个字符")
    private String nickName;

    /** 用户邮箱 */
    @Excel(name = "用户邮箱")
    @Email(message = "邮箱格式不正确")
    @Length(max = 50, message = "邮箱长度不能超过50个字符")
    private String email;

    /** 手机号码 */
    @Excel(name = "手机号码")
    @Length(max = 11, message = "手机号码长度不能超过11个字符")
    private String phonenumber;

    /** 用户性别（0男 1女） */
    @Excel(name = "用户性别", replace = { "男_0", "女_1", "未知_2" })
    private String sex;

    /** 用户头像 */
    private String avatar;

    /** 密码 */
    @ColumnMask(Masks.PASSWORD)
    private String password;

    /** 帐号状态（0正常 1停用） */
    @Excel(name = "帐号状态", replace = { "正常_0", "停用_1" })
    private String status;

    /** 最后登录IP */
    @Excel(name = "最后登录IP")
    private String loginIp;

    /** 最后登录时间 */
    @Excel(name = "最后登录时间", exportFormat = "yyyy-MM-dd HH:mm:ss")
    private Date loginDate;

    /** 部门对象 */
    @Column(ignore = true)
    private SysDept dept;

    /** 角色对象 */
    @Column(ignore = true)
    private List<SysRole> roles;

    /** 角色组 */
    @Column(ignore = true)
    private Long[] roleIds;

    /** 岗位组 */
    @Column(ignore = true)
    private Long[] postIds;

    /** 角色ID */
    @Column(ignore = true)
    private Long roleId;

}
