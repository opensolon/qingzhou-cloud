package com.qingzhou.common.core.constants;

/**
 * 用户常量信息
 * @author xm
 */
public class UserConstant {

    /**
     * 用户名长度限制
     */
    // 最小
    public static final int USERNAME_MIN_LENGTH = 2;
    // 最大
    public static final int USERNAME_MAX_LENGTH = 20;

    /**
     * 密码长度限制
     */
    // 最小
    public static final int PASSWORD_MIN_LENGTH = 5;
    // 最大
    public static final int PASSWORD_MAX_LENGTH = 20;

    /**
     * 超级管理员
     */
    // 用户 ID
    public static final long ADMIN_USER_ID = 1L;
    // 角色 ID
    public static final long ADMIN_ROLE_ID = 1L;

    /**
     * 菜单类型
     */
    // 目录
    public static final String MENU_TYPE_DIR = "M";
    // 菜单
    public static final String MENU_TYPE_MENU = "C";

    /** InnerLink组件标识 */
    public final static String INNER_LINK = "InnerLink";

    /** Layout组件标识 */
    public final static String LAYOUT = "Layout";

    /** ParentView组件标识 */
    public final static String PARENT_VIEW = "ParentView";

    /**
     * 数据权限
     */
    // 全部数据权限
    public static final String DATA_SCOPE_ALL = "1";
    // 自定数据权限
    public static final String DATA_SCOPE_CUSTOM = "2";
    // 部门数据权限
    public static final String DATA_SCOPE_DEPT = "3";
    // 部门及以下数据权限
    public static final String DATA_SCOPE_DEPT_AND_CHILD = "4";
    // 仅本人数据权限
    public static final String DATA_SCOPE_SELF = "5";
    // 部门表的别名
    public static final String DATA_SCOPE_ALIAS_DEPT = "d";
    // 用户表的别名
    public static final String DATA_SCOPE_ALIAS_USER = "u";

}
