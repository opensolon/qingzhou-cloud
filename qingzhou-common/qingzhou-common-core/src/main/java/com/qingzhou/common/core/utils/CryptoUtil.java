package com.qingzhou.common.core.utils;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;

/**
 * 加解密 工具类
 * @author xm
 **/
public class CryptoUtil {

    // RSA公钥（需与前端公钥相同）
    public static String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKoR8mX0rGKLqzcWmOzbfj64K8ZIgOdH\n" +
            "nzkXSOVOZbFu/TJhZ7rFAN+eaGkl3C4buccQd/EjEsj9ir7ijT7h96MCAwEAAQ==";

    // RSA私钥（需与前端私钥相同）
    public static String privateKey = "MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAqhHyZfSsYourNxaY\n" +
            "7Nt+PrgrxkiA50efORdI5U5lsW79MmFnusUA355oaSXcLhu5xxB38SMSyP2KvuKN\n" +
            "PuH3owIDAQABAkAfoiLyL+Z4lf4Myxk6xUDgLaWGximj20CUf+5BKKnlrK+Ed8gA\n" +
            "kM0HqoTt2UZwA5E2MzS4EI2gjfQhz5X28uqxAiEA3wNFxfrCZlSZHb0gn2zDpWow\n" +
            "cSxQAgiCstxGUoOqlW8CIQDDOerGKH5OmCJ4Z21v+F25WaHYPxCFMvwxpcw99Ecv\n" +
            "DQIgIdhDTIqD2jfYjPTY8Jj3EDGPbH2HHuffvflECt3Ek60CIQCFRlCkHpi7hthh\n" +
            "YhovyloRYsM+IS9h/0BzlEAuO0ktMQIgSPT3aFAgJYwKpqRYKlLDVcflZFCKY7u3\n" +
            "UP8iWi1Qw0Y=";

    /**
     * RSA私钥解密
     * @param text 待解密的文本
     * @return 解密后的文本
     */
    public static String rsaDecrypt(String text) {
        return getRSA().decryptStr(text, KeyType.PrivateKey);
    }

    /**
     * RSA公钥加密
     * @param text 待加密的文本
     * @return 加密后的文本
     */
    public static String rsaEncrypt(String text) {
        return getRSA().encryptBase64(text, KeyType.PublicKey);
    }

    /**
     * 获取加解密对象RSA
     * @return
     */
    private static RSA getRSA() {
        return SecureUtil.rsa(privateKey, publicKey);
    }

}
