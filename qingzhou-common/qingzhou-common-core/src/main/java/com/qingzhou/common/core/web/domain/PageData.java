package com.qingzhou.common.core.web.domain;

import java.io.Serializable;
import java.util.List;

/**
 * 分页数据
 * @author xm
 */
public class PageData<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 数据 */
    private List<T> list;

    /** 数据总量 */
    private long total;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

}
