package com.qingzhou.common.core.web.exception;

import com.qingzhou.common.core.constants.HttpConstant;
import org.noear.solon.core.exception.StatusException;

/**
 * 自定义业务异常
 * @author xm
 */
public class ServiceException extends StatusException {
    private static final long serialVersionUID = 7290971085913199343L;

    public ServiceException(String message) {
        this(message, HttpConstant.HTTP_BAD_REQUEST);
    }

    public ServiceException(String message, int code) {
        super(message, code);
    }

    public ServiceException(Throwable cause) {
        super(cause, HttpConstant.HTTP_BAD_REQUEST);
    }

}
