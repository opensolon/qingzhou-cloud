package com.qingzhou.common.core.constants;

/**
 * 数据源常量信息
 * @author xm
 */
public class DataSourceConstant {

    /**
     * 数据源
     */
    public static final String DATA_SOURCE = "data_source";

    /**
     * 子数据源（default 或 master，会做为动态源的默认源）
     */
    public static final String DATA_SOURCE_MASTER = "master";
    public static final String DATA_SOURCE_SLAVE_1 = "slave_1";

}
