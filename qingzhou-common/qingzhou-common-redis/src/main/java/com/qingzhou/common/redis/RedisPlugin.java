package com.qingzhou.common.redis;

import com.qingzhou.common.redis.config.RedisConfig;
import org.noear.solon.core.AppContext;
import org.noear.solon.core.Plugin;

/**
 * 插件扩展
 * @author xm
 */
public class RedisPlugin implements Plugin {

    /**
     * 注册Bean
     * @param context
     * @throws Throwable
     */
    @Override
    public void start(AppContext context) throws Throwable {
        // 生成Bean，并触发身上的注解处理（比如类上有 @Controller 注解；则会执行 @Controller 对应的处理）
        context.beanMake(RedisConfig.class);
    }

}
