package com.qingzhou.common.security.config.properties;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 安全Properties
 * @author xm
 */
@Data
public class SecurityProperties {

    /**
     * IP黑名单
     */
    private String[] ips = new String[]{};

    /**
     * 放行白名单
     */
    private String[] whites = new String[]{};

}
