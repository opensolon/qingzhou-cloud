package com.qingzhou.common.datasource.listener;

import com.qingzhou.common.core.constants.DataSourceConstant;
import com.qingzhou.common.datasource.config.MybatisFlexConfig;
import org.noear.solon.annotation.Component;
import org.noear.solon.core.event.AppBeanLoadEndEvent;
import org.noear.solon.core.event.EventListener;

/**
 * 应用 Bean加载完成事件
 * @author xm
 */
@Component
public class AppLoadEndEventListener implements EventListener<AppBeanLoadEndEvent> {

    @Override
    public void onEvent(AppBeanLoadEndEvent event) throws Throwable {
        event.app().context().beanForeach((k, v) -> {
            // 如果引入了数据源，配置一下 Mybatis-Flex
            if(DataSourceConstant.DATA_SOURCE.equals(k)) {
                event.app().context().beanMake(MybatisFlexConfig.class);
            }
        });
    }

}
