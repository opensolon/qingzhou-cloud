package com.qingzhou.common.datasource.annotation;

import com.qingzhou.common.datasource.aspect.DataScopeAspect;
import org.noear.solon.annotation.Around;

import java.lang.annotation.*;

/**
 * 数据权限过滤注解（方法的形参必须是继承自 BaseEntity的实体类）
 * @author xm
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Around(DataScopeAspect.class)
public @interface QzDataScope {

    /**
     * 部门表的别名
     */
    String deptAlias() default "";

    /**
     * 用户表的别名
     */
    String userAlias() default "";

}
