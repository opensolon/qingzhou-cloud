package com.qingzhou.common.datasource.aspect;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.qingzhou.common.core.enums.DictEnum;
import com.qingzhou.common.datasource.annotation.QzLog;
import com.qingzhou.common.security.utils.SecurityUtil;
import com.qingzhou.system.api.RemoteSysLogService;
import com.qingzhou.system.api.domain.SysOperLog;
import org.noear.nami.annotation.NamiClient;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.Filter;
import org.noear.solon.core.handle.FilterChain;
import org.noear.solon.core.wrap.MethodWrap;

/**
 * 操作日志记录
 * @author xm
 */
public class LogAspect implements Filter {

    @NamiClient
    RemoteSysLogService remoteSysLogService;

    @Override
    public void doFilter(Context ctx, FilterChain chain) throws Throwable {
        chain.doFilter(ctx);

        MethodWrap method = ctx.action().method();
        QzLog qzLog = method.getAnnotation(QzLog.class);

        SysOperLog sol = new SysOperLog();
        sol.setTitle(qzLog.title());
        sol.setBusinessType(qzLog.businessType().ordinal());
        sol.setMethod(method.getDeclaringClz() + "." + method.getName() + "()");
        sol.setRequestMethod(ctx.method());
        sol.setOperatorType(SecurityUtil.getDeviceType());
        sol.setOperName(SecurityUtil.getUserName());
        sol.setOperUrl(ctx.path());
        sol.setOperIp(ctx.realIp());
        if(qzLog.isSaveRequestData()) {
            sol.setOperParam(StrUtil.isNotBlank(ctx.body()) ? ctx.body() : ctx.paramMap().toString());
        }
        if(qzLog.isSaveResponseData()) {
            sol.setJsonResult(JSONUtil.toJsonStr(ctx.result));
        }
        if(ObjectUtil.isNull(ctx.errors)) {
            sol.setStatus(DictEnum.COMMON_SUCCESS_FAIL.SUCCESS.getValue());
        } else {
            sol.setStatus(DictEnum.COMMON_SUCCESS_FAIL.FAIL.getValue());
            sol.setErrorMsg(ctx.errors.toString());
        }
        remoteSysLogService.saveOperLog(sol);
    }

}
