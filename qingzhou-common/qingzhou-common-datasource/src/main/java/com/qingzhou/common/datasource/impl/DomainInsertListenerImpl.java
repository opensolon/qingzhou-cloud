package com.qingzhou.common.datasource.impl;

import com.mybatisflex.annotation.InsertListener;
import com.qingzhou.common.core.web.domain.BaseEntity;
import com.qingzhou.common.security.utils.SecurityUtil;

import java.util.Date;

/**
 * 实体类插入 监听
 * @author xm
 */
public class DomainInsertListenerImpl<T extends BaseEntity> implements InsertListener {

    /**
     * 实体类执行插入时，自动赋值创建者和创建时间
     * @param entity
     */
    @SuppressWarnings("unchecked")
    @Override
    public void onInsert(Object entity) {
        if(entity instanceof BaseEntity) {
            T t = (T) entity;
            t.setCreateBy(SecurityUtil.getUserName());
            t.setCreateTime(new Date());
        }
    }

}
