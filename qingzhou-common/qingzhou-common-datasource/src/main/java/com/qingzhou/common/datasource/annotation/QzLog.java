package com.qingzhou.common.datasource.annotation;

import com.qingzhou.common.core.enums.BusinessType;
import com.qingzhou.common.datasource.aspect.LogAspect;
import org.noear.solon.annotation.Addition;

import java.lang.annotation.*;

/**
 * 操作日志记录注解
 * @author xm
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Addition(LogAspect.class)
public @interface QzLog {

    /**
     * 操作模块
     */
    String title() default "";

    /**
     * 业务操作类型
     */
    BusinessType businessType() default BusinessType.OTHER;

    /**
     * 是否保存请求参数
     */
    boolean isSaveRequestData() default true;

    /**
     * 是否保存响应数据
     */
    boolean isSaveResponseData() default true;

}
