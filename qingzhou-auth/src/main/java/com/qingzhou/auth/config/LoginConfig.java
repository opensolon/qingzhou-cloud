package com.qingzhou.auth.config;

import lombok.Data;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

/**
 * 验证码配置
 * @author xm
 */
@Data
@Configuration
@Inject(value = "${qingzhou.login}", autoRefreshed = true)
public class LoginConfig {

    /**
     * 登录失败最大次数
     */
    private long errorCount;

    /**
     * 登录失败达最大次数后的锁定时长（单位：分钟）
     */
    private long lockTime;

}
