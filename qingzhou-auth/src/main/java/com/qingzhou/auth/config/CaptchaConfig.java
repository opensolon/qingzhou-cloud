package com.qingzhou.auth.config;

import lombok.Data;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

/**
 * 验证码配置
 * @author xm
 */
@Data
@Configuration
@Inject(value = "${qingzhou.auth}", autoRefreshed = true)
public class CaptchaConfig {

    /**
     * 验证码开关
     */
    private Boolean captchaEnabled;

    /**
     * 干扰类型: line 线段干扰、circle 圆圈干扰、shear 扭曲干扰
     */
    private String captchaInvadeType;

    /**
     * 验证码类型: math 四则运算、char 字符
     */
    private String captchaType;

}
