package com.qingzhou.auth.controller;

import com.qingzhou.auth.domain.LoginBody;
import com.qingzhou.auth.domain.RegisterBody;
import com.qingzhou.auth.service.LoginService;
import com.qingzhou.common.core.enums.DeviceType;
import org.noear.solon.annotation.*;
import org.noear.solon.core.handle.Result;

import java.util.Map;

/**
 * 登录 注册 操作处理
 * @author xm
 */
@Controller
@Mapping
public class AuthController {

    @Inject
    LoginService loginService;

    /**
     * 登录
     */
    @Post
    @Mapping("login")
    public Map<String, Object> login(@Body LoginBody loginBody) {
        return loginService.login(loginBody, DeviceType.PC.getCode());
    }

    /**
     * 获取登录用户信息
     */
    @Get
    @Mapping("getUserInfo")
    public Map<String, Object> getUserInfo() {
        return loginService.getUserInfo();
    }

    /**
     * 登出
     */
    @Delete
    @Mapping("logout")
    public Result<Void> logout() {
        loginService.logout();
        return Result.succeed();
    }

    /**
     * 注册
     */
    @Post
    @Mapping("register")
    public Result<Void> register(@Body RegisterBody registerBody) {
        loginService.register(registerBody, DeviceType.PC.getCode());
        return Result.succeed();
    }

}
