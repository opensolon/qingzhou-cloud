package com.qingzhou.auth.controller;

import com.qingzhou.auth.service.CaptchaService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Result;

import java.util.Map;

/**
 * 验证码 操作处理
 * @author xm
 */
@Controller
@Mapping("captcha")
public class CaptchaController {

    @Inject
    CaptchaService captchaService;

    /**
     * 获取验证码
     */
    @Get
    @Mapping
    public Result<Map<String, Object>> captcha() {
        return captchaService.getCaptcha();
    }

}
