package com.qingzhou.monitor;

import org.noear.solon.Solon;
import org.noear.solon.admin.client.annotation.EnableAdminClient;
import org.noear.solon.admin.server.annotation.EnableAdminServer;
import org.noear.solon.annotation.SolonMain;

/**
 * 监控中心
 * @author xm
 */
@EnableAdminClient
@EnableAdminServer
@SolonMain
public class QingZhouMonitorApplication {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Solon.start(QingZhouMonitorApplication.class, args);
        long times = System.currentTimeMillis() - start;
        System.out.println("(♥◠‿◠)ﾉﾞ  监控中心启动成功，耗时:【" + times + "ms】  ლ(´ڡ`ლ)ﾞ");
    }

}
