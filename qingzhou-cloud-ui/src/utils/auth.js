import Cookies from 'js-cookie'
import store from '@/store'

/**
 * 认证相关
 */

const TokenKey = 'Admin-Token'
const ExpiresInKey = 'Admin-Expires-In'

/**
 * token缓存
 */
export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getExpiresIn() {
  return Cookies.get(ExpiresInKey) || -1
}

export function setExpiresIn(time) {
  return Cookies.set(ExpiresInKey, time)
}

export function removeExpiresIn() {
  return Cookies.remove(ExpiresInKey)
}

/**
 * 验证用户是否具备某权限
 * @param {*} permission 权限字符
 * @returns {Boolean}
 */
export function hasPermi(permission) {
  return authPermission(permission)
}

/**
 * 验证用户是否含有指定权限，只需包含其中一个
 * @param {*} permissions 权限字符数组
 * @returns {Boolean}
 */
export function hasPermiOr(permissions) {
  return permissions.some(item => {
    return authPermission(item)
  })
}

/**
 * 验证用户是否含有指定权限，必须全部拥有
 * @param {*} permissions 权限字符数组
 * @returns {Boolean}
 */
export function hasPermiAnd(permissions) {
  return permissions.every(item => {
    return authPermission(item)
  })
}

/**
 * 验证用户是否具备某角色
 * @param {*} role 角色
 * @returns {Boolean}
 */
export function hasRole(role) {
  return authRole(role)
}

/**
 * 验证用户是否含有指定角色，只需包含其中一个
 * @param {*} roles 角色数组
 * @returns {Boolean}
 */
export function hasRoleOr(roles) {
  return roles.some(item => {
    return authRole(item)
  })
}

/**
 * 验证用户是否含有指定角色，必须全部拥有
 * @param {*} roles 角色数组
 * @returns {Boolean}
 */
export function hasRoleAnd(roles) {
  return roles.every(item => {
    return authRole(item)
  })
}

/**
 * 字符权限校验
 * @param {Array} value 校验值
 * @returns {Boolean}
 */
export function checkPermi(value) {
  if (value && value instanceof Array && value.length > 0) {
    const permissions = store.getters && store.getters.permissions
    const permissionDatas = value
    const all_permission = '*:*:*'

    const hasPermission = permissions.some(permission => {
      return all_permission === permission || permissionDatas.includes(permission)
    })

    if (!hasPermission) {
      return false
    }
    return true
  } else {
    console.error(`need roles! Like checkPermi="['system:user:add','system:user:edit']"`)
    return false
  }
}

/**
 * 角色权限校验
 * @param {Array} value 校验值
 * @returns {Boolean}
 */
export function checkRole(value) {
  if (value && value instanceof Array && value.length > 0) {
    const roles = store.getters && store.getters.roles
    const permissionRoles = value
    const super_admin = 'admin'

    const hasRole = roles.some(role => {
      return super_admin === role || permissionRoles.includes(role)
    })

    if (!hasRole) {
      return false
    }
    return true
  } else {
    console.error(`need roles! Like checkRole="['admin','editor']"`)
    return false
  }
}

function authPermission(permission) {
  const all_permission = '*:*:*'
  const permissions = store.getters && store.getters.permissions
  if (permission && permission.length > 0) {
    return permissions.some(v => {
      return all_permission === v || v === permission
    })
  } else {
    return false
  }
}

function authRole(role) {
  const super_admin = 'admin'
  const roles = store.getters && store.getters.roles
  if (role && role.length > 0) {
    return roles.some(v => {
      return super_admin === v || v === role
    })
  } else {
    return false
  }
}
