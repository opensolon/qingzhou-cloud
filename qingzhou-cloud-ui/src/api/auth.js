import request from '@/utils/request'
import { encrypt } from '@/utils/jsencrypt'

// 登录方法
export function login(username, password, code, uuid) {
  // 加密密码
  password = encrypt(password)
  return request({
    url: '/auth/login',
    headers: {
      isToken: false
    },
    method: 'post',
    data: { username, password, code, uuid }
  })
}

// 注册方法
export function register(data) {
  // 加密密码
  const d = { ...data }
  d.password = encrypt(d.password)
  delete d.confirmPassword
  return request({
    url: '/auth/register',
    headers: {
      isToken: false
    },
    method: 'post',
    data: d
  })
}

// 获取登录用户信息
export function getUserInfo() {
  return request({
    url: '/auth/getUserInfo',
    method: 'get'
  })
}

// 刷新方法
export function refreshToken() {
  return request({
    url: '/auth/refresh',
    method: 'post'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/auth/logout',
    method: 'delete'
  })
}

// 获取验证码
export function getCaptcha() {
  return request({
    url: '/auth/captcha',
    headers: {
      isToken: false
    },
    method: 'get',
    timeout: 20000
  })
}
