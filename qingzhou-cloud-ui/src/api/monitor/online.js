import request from '@/utils/request'

// 查询在线用户列表
export function listOnline(query) {
  return request({
    url: '/system/online/list',
    method: 'get',
    params: query
  })
}

// 强退所有用户
export function forceLogoutAll(userId) {
  return request({
    url: '/system/online/forceLogoutAll/' + userId,
    method: 'delete'
  })
}

// 强退用户
export function forceLogout(token) {
  return request({
    url: '/system/online/' + token,
    method: 'delete'
  })
}
