import Vue from 'vue'
import dialogDrag from './dialog/drag'
import dialogDragHeight from './dialog/dragHeight'
import dialogDragWidth from './dialog/dragWidth'
import clipboard from './module/clipboard'
import hasPermi from './permission/hasPermi'
import hasRole from './permission/hasRole'

const install = function(Vue) {
  Vue.directive('dialog-drag', dialogDrag)
  Vue.directive('dialog-drag-height', dialogDragHeight)
  Vue.directive('dialog-drag-width', dialogDragWidth)
  Vue.directive('clipboard', clipboard)
  Vue.directive('has-permi', hasPermi)
  Vue.directive('has-role', hasRole)
}

if (window.Vue) {
  window['has-permi'] = hasPermi
  window['has-role'] = hasRole
  Vue.use(install)
}

export default install
