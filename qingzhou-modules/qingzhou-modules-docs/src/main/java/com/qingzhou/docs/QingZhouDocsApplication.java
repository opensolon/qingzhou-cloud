package com.qingzhou.docs;

import org.noear.solon.Solon;
import org.noear.solon.annotation.SolonMain;

/**
 * 接口文档模块
 * @author xm
 */
//@EnableAdminClient
@SolonMain
public class QingZhouDocsApplication {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Solon.start(QingZhouDocsApplication.class, args);
        long times = System.currentTimeMillis() - start;
        System.out.println("(♥◠‿◠)ﾉﾞ  接口文档模块启动成功，耗时:【" + times + "ms】  ლ(´ڡ`ლ)ﾞ");
    }

}
