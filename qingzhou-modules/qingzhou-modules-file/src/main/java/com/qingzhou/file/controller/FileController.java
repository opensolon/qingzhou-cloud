package com.qingzhou.file.controller;

import com.qingzhou.file.service.FileService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;
import org.noear.solon.core.handle.Result;
import org.noear.solon.core.handle.UploadedFile;

/**
 * 文件 操作处理
 * @author xm
 */
@Controller
@Mapping("upload")
public class FileController {

    @Inject
    FileService fileService;

    /**
     * 单文件上传
     * @param file
     * @return
     */
    @Post
    @Mapping("file")
    public Result<?> file(UploadedFile file) {
        return fileService.uploadFile(file);
    }

    /**
     * 多文件上传
     * @param file
     * @return
     */
    @Post
    @Mapping("files")
    public Result<?> files(UploadedFile... file) {
        return fileService.uploadFiles(file);
    }

}
