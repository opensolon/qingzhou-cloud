package com.qingzhou.file.config;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import java.util.List;

/**
 * 文件配置
 * @author xm
 */
@Data
@Configuration
@Inject(value = "${qingzhou.file}", autoRefreshed = true)
public class FileConfig {

    /**
     * 使用的文件服务类型: s3 本地文件存储、aliyun 阿里云对象存储、qiniu 七牛云对象存储、minio
     */
    private String type;

    /**
     * 允许上传的文件大小（单位：MB，设为-1不限制）
     */
    private long maxSize;

    /**
     * 允许上传的文件类型
     */
    private String fileType;


    public List<String> getFileType() {
        return StrUtil.split(fileType, ",", true, true);
    }

    /**
     * 是否超限
     * @param size 要判断的文件的大小，单位：B
     * @return
     */
    public boolean isLimitSize(long size) {
        long s = maxSize * 1024 * 1024;
        return size > s;
    }

}
