package com.qingzhou.gen.controller;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.qingzhou.common.core.enums.BusinessType;
import com.qingzhou.common.datasource.annotation.QzLog;
import com.qingzhou.gen.service.IGenTableService;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.common.core.web.domain.PageData;
import com.qingzhou.gen.domain.GenTable;
import org.noear.solon.annotation.*;
import org.noear.solon.auth.annotation.AuthPermissions;
import org.noear.solon.core.handle.Result;
import org.noear.solon.data.annotation.Tran;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;
import java.util.Map;

/**
 * 代码生成 操作处理
 * @author xm
 */
@Controller
@Mapping("table")
public class GenTableController extends BaseController {

    @Inject
    IGenTableService genTableService;

    /**
     * 查询数据库列表
     */
    @AuthPermissions("tool:gen:list")
    @Get
    @Mapping("tables")
    public PageData<GenTable> tables(Page<GenTable> page, GenTable genTable) {
        startPage(page);
        List<GenTable> list = genTableService.tables(genTable);
        return getPageData(list);
    }

    /**
     * 查询代码生成列表
     */
    @AuthPermissions("tool:gen:list")
    @Get
    @Mapping("list")
    public PageData<GenTable> list(Page<GenTable> page, GenTable genTable) {
        QueryWrapper qw = QueryWrapper.create();
        qw.and(GenTable::getTableName).like(genTable.getTableName());
        qw.and(GenTable::getTableComment).like(genTable.getTableComment());
        Page<GenTable> result = genTableService.page(page, qw);
        return getPageData(result);
    }

    /**
     * 查询代码生成详情
     */
    @AuthPermissions("tool:gen:query")
    @Get
    @Mapping("{tableId}")
    public Map<String, Object> info(@Path Long tableId) {
        return genTableService.info(tableId);
    }

    /**
     * 预览代码生成
     */
    @AuthPermissions("tool:gen:preview")
    @Get
    @Mapping("preview/{tableId}")
    public Map<String, String> preview(@Path Long tableId) {
        return genTableService.preview(tableId);
    }

    /**
     * 新增代码生成（导入表）
     */
    @QzLog(title = "代码生成", businessType = BusinessType.INSERT)
    @AuthPermissions("tool:gen:add")
    @Tran
    @Post
    @Mapping
    public Result<Void> add(String tables) {
        genTableService.add(tables);
        return Result.succeed();
    }

    /**
     * 修改代码生成
     */
    @QzLog(title = "代码生成", businessType = BusinessType.UPDATE)
    @AuthPermissions("tool:gen:edit")
    @Tran
    @Put
    @Mapping
    public Result<Void> edit(@Body @Validated GenTable genTable) {
        return toResult(genTableService.edit(genTable));
    }

    /**
     * 删除代码生成
     */
    @QzLog(title = "代码生成", businessType = BusinessType.DELETE)
    @AuthPermissions("tool:gen:remove")
    @Tran
    @Delete
    @Mapping("{tableIds}")
    public Result<Void> delete(@Path Long[] tableIds) {
        genTableService.delete(tableIds);
        return Result.succeed();
    }

    /**
     * 同步数据库
     */
    @QzLog(title = "代码生成", businessType = BusinessType.UPDATE)
    @AuthPermissions("tool:gen:edit")
    @Get
    @Mapping("synchDb/{tableName}")
    public Result<Void> synchDb(@Path String tableName) {
        genTableService.synchDb(tableName);
        return Result.succeed();
    }

    /**
     * 生成代码（自定义路径）
     */
    @QzLog(title = "代码生成", businessType = BusinessType.GENCODE)
    @AuthPermissions("tool:gen:code")
    @Get
    @Mapping("genCode/{tableName}")
    public Result<Void> genCode(@Path String tableName) {
        genTableService.genCode(tableName);
        return Result.succeed();
    }

    /**
     * 批量生成代码
     */
    @QzLog(title = "代码生成", businessType = BusinessType.GENCODE)
    @AuthPermissions("tool:gen:code")
    @Get
    @Mapping("batchGenCode")
    public void batchGenCode(String tables) {
        genTableService.batchGenCode(tables);
    }

}
