package com.qingzhou.gen.config;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import java.util.List;

/**
 * 代码生成配置
 * @author xm
 */
@Data
@Configuration
@Inject(value = "${qingzhou.gen}", autoRefreshed = true)
public class GenConfig {

    /** 作者 */
    public String author;

    /** 生成包路径 */
    public String packageName;

    /** 自动去除表前缀，默认是false */
    public boolean autoRemovePre;

    /** 表前缀（类名不会包含表前缀） */
    public String tablePrefix;

    /** 忽略的表前缀（不会查询这些前缀的表） */
    public String ignoreTablePrefix;


    public List<String> getIgnoreTablePrefix() {
        return StrUtil.split(ignoreTablePrefix, ",", true, true);
    }

}
