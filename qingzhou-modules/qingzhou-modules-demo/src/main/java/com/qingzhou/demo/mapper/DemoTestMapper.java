package com.qingzhou.demo.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.demo.domain.DemoTest;

/**
 * 演示 数据层
 * @author xm
 */
public interface DemoTestMapper extends BaseMapper<DemoTest> {

}
