package com.qingzhou.schedule.task;

import com.qingzhou.system.api.RemoteSysUserService;
import org.noear.nami.annotation.NamiClient;
import org.noear.solon.annotation.Component;
import org.noear.solon.cloud.annotation.CloudJob;

/**
 * 任务调度（计划任务）
 * @author xm
 */
//@Component
public class QzTask {

    @NamiClient
    RemoteSysUserService remoteSysUserService;

    /**
     * 计划任务（定时任务）
     */
    @CloudJob(name = "job001", cron7x = "3s")
    public void job001() {
        System.out.println("我是 job001 （3s）");
    }

    /**
     * 计划任务（cron表达式）
     */
    @CloudJob(name = "job002", cron7x = "0/10 * * * * ? *")
    public void job002() {
        System.out.println("我是 job002 （0/10 * * * * ? *）");
//        remoteSysUserService.doSomething();
    }

}
