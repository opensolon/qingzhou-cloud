package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.common.core.web.domain.entity.SysRole;

import java.util.List;

/**
 * 系统角色 数据层
 * @author xm
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 查询角色列表
     * @param sysRole
     * @param sysRole
     * @return
     */
    List<SysRole> selectRoleList(SysRole sysRole, int delFlag);

    /**
     * 根据用户ID查询角色
     * @param userId 用户ID
     * @param delFlag
     * @return
     */
    List<SysRole> selectRolePermissionByUserId(Long userId, int delFlag);

    /**
     * 根据用户名查询角色
     * @param userName  用户名
     * @param delFlag
     * @return
     */
    List<SysRole> selectRolesByUserName(String userName, int delFlag);

    /**
     * 通过角色ID查询角色
     * @param roleId 角色ID
     * @param delFlag
     * @return
     */
    SysRole selectRoleById(Long roleId, int delFlag);

    /**
     * 校验角色名称是否唯一
     * @param roleName 角色名称
     * @param delFlag
     * @return
     */
    SysRole checkRoleNameUnique(String roleName, int delFlag);

    /**
     * 校验角色权限是否唯一
     * @param roleKey 角色权限
     * @param delFlag
     * @return
     */
    SysRole checkRoleKeyUnique(String roleKey, int delFlag);

}
