package com.qingzhou.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import com.qingzhou.common.core.constants.HttpConstant;
import com.qingzhou.common.core.constants.UserConstant;
import com.qingzhou.common.core.enums.DictEnum;
import com.qingzhou.common.core.web.exception.ServiceException;
import com.qingzhou.common.security.utils.SecurityUtil;
import com.qingzhou.common.core.web.domain.entity.SysRole;
import com.qingzhou.common.core.web.domain.entity.SysUser;
import com.qingzhou.system.domain.SysMenu;
import com.qingzhou.system.domain.vo.MetaVo;
import com.qingzhou.system.domain.vo.RouterVo;
import com.qingzhou.system.domain.vo.TreeSelect;
import com.qingzhou.system.mapper.SysMenuMapper;
import com.qingzhou.system.mapper.SysRoleMenuMapper;
import com.qingzhou.system.service.ISysMenuService;
import com.qingzhou.system.service.ISysRoleService;
import org.apache.ibatis.solon.annotation.Db;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 系统菜单 服务层实现
 * @author xm
 */
@Component
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

    @Db
    SysMenuMapper sysMenuMapper;

    @Db
    SysRoleMenuMapper sysRoleMenuMapper;

    @Inject
    ISysRoleService sysRoleService;

    /**
     * 根据用户查询系统菜单列表
     * @param menu 菜单信息
     * @param userId 用户ID
     * @return
     */
    @Override
    public List<SysMenu> selectMenuList(SysMenu menu, Long userId) {
        List<SysMenu> menuList = null;
        // 管理员显示所有菜单信息
        if (SecurityUtil.isAdmin(userId)) {
            menuList = sysMenuMapper.selectMenuList(menu, DictEnum.DEL_FLAG.OK.getValue());
        } else {
            menuList = sysMenuMapper.selectMenuListByUserId(menu, userId, DictEnum.DEL_FLAG.OK.getValue());
        }
        return menuList;
    }

    /**
     * 构建前端所需要下拉树结构
     * @param menus 菜单列表
     * @return
     */
    @Override
    public List<TreeSelect> buildMenuTreeSelect(List<SysMenu> menus) {
        List<SysMenu> menuTrees = buildMenuTree(menus);
        return menuTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    /**
     * 根据用户查询系统菜单列表
     * @param userId 用户ID
     * @return
     */
    @Override
    public List<SysMenu> selectMenuList(Long userId) {
        return selectMenuList(new SysMenu(), userId);
    }

    /**
     * 根据用户 ID查询菜单树信息
     * @param userId
     * @return
     */
    @Override
    public List<SysMenu> selectMenuTreeByUserId(Long userId) {
        List<SysMenu> menus = null;
        String[] menuTypes = new String[]{ UserConstant.MENU_TYPE_DIR, UserConstant.MENU_TYPE_MENU };
        // 超级管理员查询所有菜单
        if (SecurityUtil.isAdmin(userId)) {
            menus = sysMenuMapper.selectMenuTreeAll(menuTypes, DictEnum.COMMON_STATUS.OK.getValue(), DictEnum.DEL_FLAG.OK.getValue());
        } else {
            menus = sysMenuMapper.selectMenuTreeByUserId(menuTypes, userId, DictEnum.COMMON_STATUS.OK.getValue(), DictEnum.DEL_FLAG.OK.getValue());
        }
        return getChildPerms(menus, 0);
    }

    /**
     * 根据角色ID查询菜单树信息
     * @param roleId 角色ID
     * @return
     */
    @Override
    public List<Long> selectMenuListByRoleId(Long roleId) {
        SysRole role = sysRoleService.selectRoleById(roleId);
        return sysMenuMapper.selectMenuListByRoleId(roleId, role.isMenuCheckStrictly(), DictEnum.DEL_FLAG.OK.getValue());
    }

    /**
     * 构建前端路由所需要的菜单
     * @param menus
     * @return
     */
    @Override
    public List<RouterVo> buildMenus(List<SysMenu> menus) {
        List<RouterVo> routers = new LinkedList<>();
        for (SysMenu menu : menus) {
            RouterVo router = new RouterVo();
            router.setHidden("1".equals(menu.getVisible()));
            router.setName(getRouteName(menu));
            router.setPath(getRouterPath(menu));
            router.setComponent(getComponent(menu));
            router.setQuery(menu.getQuery());
            router.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon(), StrUtil.equals(DictEnum.COMMON_YES_NO.NO.getValue(), menu.getIsCache()), menu.getPath()));
            List<SysMenu> cMenus = menu.getChildren();
            if (!cMenus.isEmpty() && UserConstant.MENU_TYPE_DIR.equals(menu.getMenuType())) {
                router.setAlwaysShow(true);
                router.setRedirect("noRedirect");
                router.setChildren(buildMenus(cMenus));
            } else if (isMenuFrame(menu)) {
                router.setMeta(null);
                List<RouterVo> childrenList = new ArrayList<RouterVo>();
                RouterVo children = new RouterVo();
                children.setPath(menu.getPath());
                children.setComponent(menu.getComponent());
                children.setName(StrUtil.upperFirst(menu.getPath()));
                children.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon(), StrUtil.equals(DictEnum.COMMON_YES_NO.NO.getValue(), menu.getIsCache()), menu.getPath()));
                children.setQuery(menu.getQuery());
                childrenList.add(children);
                router.setChildren(childrenList);
            } else if (menu.getParentId().intValue() == 0 && isInnerLink(menu)) {
                router.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon()));
                router.setPath("/");
                List<RouterVo> childrenList = new ArrayList<RouterVo>();
                RouterVo children = new RouterVo();
                String routerPath = innerLinkReplaceEach(menu.getPath());
                children.setPath(routerPath);
                children.setComponent(UserConstant.INNER_LINK);
                children.setName(StrUtil.upperFirst(routerPath));
                children.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon(), menu.getPath()));
                childrenList.add(children);
                router.setChildren(childrenList);
            }
            routers.add(router);
        }
        return routers;
    }

    /**
     * 获取菜单数据权限
     * @param sysUser
     * @return
     */
    @Override
    public Set<String> getMenuPermission(SysUser sysUser) {
        Set<String> perms = new HashSet<>();
        // 管理员拥有所有权限
        if (SecurityUtil.isAdmin(sysUser.getUserId())) {
            perms.add("*:*:*");
        } else {
            List<SysRole> roles = sysUser.getRoles();
            if (!CollUtil.isEmpty(roles)) {
                // 多角色设置permissions属性，以便数据权限匹配权限
                for (SysRole role : roles) {
                    List<String> list = sysMenuMapper.selectMenuPermsByRoleId(role.getRoleId(), DictEnum.COMMON_STATUS.OK.getValue(), DictEnum.DEL_FLAG.OK.getValue());
                    Set<String> rolePerms = new HashSet<>();
                    for (String perm : list) {
                        if (StrUtil.isNotEmpty(perm)) {
                            rolePerms.addAll(Arrays.asList(perm.trim().split(",")));
                        }
                    }
                    role.setPermissions(rolePerms);
                    perms.addAll(rolePerms);
                }
            } else {
                List<String> list = sysMenuMapper.selectMenuPermsByUserId(sysUser.getUserId(), DictEnum.COMMON_STATUS.OK.getValue(), DictEnum.DEL_FLAG.OK.getValue());
                Set<String> permsSet = new HashSet<>();
                for (String perm : list) {
                    if (StrUtil.isNotEmpty(perm)) {
                        permsSet.addAll(Arrays.asList(perm.trim().split(",")));
                    }
                }
                perms.addAll(permsSet);
            }
        }
        return perms;
    }

    /**
     * 新增系统菜单
     * @param sysMenu
     * @return
     */
    @Override
    public int add(SysMenu sysMenu) {
        if (!checkMenuNameUnique(sysMenu)) {
            throw new ServiceException("菜单名称已存在");
        } else if (DictEnum.COMMON_YES_NO.YES.getValue().equals(sysMenu.getIsFrame())
                && !StrUtil.startWithAny(sysMenu.getPath(), HttpConstant.HTTP, HttpConstant.HTTPS)) {
            throw new ServiceException("地址必须以http(s)://开头");
        }
        return sysMenuMapper.insert(sysMenu);
    }

    /**
     * 修改系统菜单
     * @param sysMenu
     * @return
     */
    @Override
    public int edit(SysMenu sysMenu) {
        if (!checkMenuNameUnique(sysMenu)) {
            throw new ServiceException("菜单名称已存在");
        } else if (DictEnum.COMMON_YES_NO.YES.getValue().equals(sysMenu.getIsFrame())
                && !StrUtil.startWithAny(sysMenu.getPath(), HttpConstant.HTTP, HttpConstant.HTTPS)) {
            throw new ServiceException("地址必须以http(s)://开头");
        } else if (sysMenu.getMenuId().equals(sysMenu.getParentId())) {
            throw new ServiceException("上级菜单不能选择自己");
        }
        return sysMenuMapper.update(sysMenu);
    }

    /**
     * 删除系统菜单
     * @param menuId
     */
    @Override
    public int delete(Long menuId) {
        QueryWrapper qw = QueryWrapper.create();
        qw.where(SysMenu::getParentId).eq(menuId);
        long l = sysMenuMapper.selectCountByQuery(qw);
        if (l > 0) {
            throw new ServiceException("存在子菜单，不允许删除");
        }
        if (sysRoleMenuMapper.checkMenuExistRole(menuId) > 0) {
            throw new ServiceException("菜单已分配，不允许删除");
        }
        return sysMenuMapper.deleteById(menuId);
    }

    /**
     * 校验菜单名称是否唯一
     * @param sysMenu 菜单信息
     * @return
     */
    public boolean checkMenuNameUnique(SysMenu sysMenu) {
        long menuId = sysMenu.getMenuId() == null ? -1L : sysMenu.getMenuId();
        QueryWrapper qw = QueryWrapper.create();
        qw.and(SysMenu::getMenuName).eq(sysMenu.getMenuName());
        qw.and(SysMenu::getParentId).eq(sysMenu.getParentId());
        qw.limit(1);
        SysMenu sm = sysMenuMapper.selectOneByQuery(qw);
        return ObjectUtil.isNull(sm) || sm.getMenuId() == menuId;
    }

    /**
     * 构建前端所需要树结构
     * @param menus 菜单列表
     * @return
     */
    public List<SysMenu> buildMenuTree(List<SysMenu> menus) {
        List<SysMenu> returnList = new ArrayList<>();
        List<Long> tempList = menus.stream().map(SysMenu::getMenuId).collect(Collectors.toList());
        for (SysMenu menu : menus) {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(menu.getParentId())) {
                recursionFn(menus, menu);
                returnList.add(menu);
            }
        }
        if (returnList.isEmpty()) {
            returnList = menus;
        }
        return returnList;
    }

    /**
     * 根据父节点的ID获取所有子节点
     * @param list 分类表
     * @param parentId 传入的父节点ID
     * @return String
     */
    public List<SysMenu> getChildPerms(List<SysMenu> list, int parentId) {
        List<SysMenu> returnList = new ArrayList<>();
        for (SysMenu t : list) {
            // 根据传入的某个父节点ID，遍历该父节点的所有子节点
            if (t.getParentId() == parentId) {
                recursionFn(list, t);
                returnList.add(t);
            }
        }
        return returnList;
    }

    /**
     * 递归列表
     * @param list
     * @param t
     */
    private void recursionFn(List<SysMenu> list, SysMenu t) {
        // 得到子节点列表
        List<SysMenu> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysMenu tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     * @param list
     * @param t
     * @return
     */
    private List<SysMenu> getChildList(List<SysMenu> list, SysMenu t) {
        List<SysMenu> tlist = new ArrayList<>();
        for (SysMenu n : list) {
            if (n.getParentId().longValue() == t.getMenuId().longValue()) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     * @param list
     * @param t
     * @return
     */
    private boolean hasChild(List<SysMenu> list, SysMenu t) {
        return getChildList(list, t).size() > 0;
    }

    /**
     * 获取路由名称
     * @param menu 菜单信息
     * @return
     */
    public String getRouteName(SysMenu menu) {
        String routerName = StrUtil.upperFirst(menu.getPath());
        // 非外链并且是一级目录（类型为目录）
        if (isMenuFrame(menu)) {
            routerName = StrUtil.EMPTY;
        }
        return routerName;
    }

    /**
     * 获取路由地址
     * @param menu 菜单信息
     * @return
     */
    public String getRouterPath(SysMenu menu) {
        String routerPath = menu.getPath();
        // 内链打开外网方式
        if (menu.getParentId().intValue() != 0 && isInnerLink(menu)) {
            routerPath = innerLinkReplaceEach(routerPath);
        }
        // 非外链并且是一级目录（类型为目录）
        if (0 == menu.getParentId().intValue()
                && UserConstant.MENU_TYPE_DIR.equals(menu.getMenuType())
                && DictEnum.COMMON_YES_NO.NO.getValue().equals(menu.getIsFrame())) {
            routerPath = "/" + menu.getPath();
        } else if (isMenuFrame(menu)) {
            // 非外链并且是一级目录（类型为菜单）
            routerPath = "/";
        }
        return routerPath;
    }

    /**
     * 获取组件信息
     *
     * @param menu 菜单信息
     * @return 组件信息
     */
    public String getComponent(SysMenu menu) {
        String component = UserConstant.LAYOUT;
        if (StrUtil.isNotEmpty(menu.getComponent()) && !isMenuFrame(menu)) {
            component = menu.getComponent();
        } else if (StrUtil.isEmpty(menu.getComponent()) && menu.getParentId().intValue() != 0 && isInnerLink(menu)) {
            component = UserConstant.INNER_LINK;
        } else if (StrUtil.isEmpty(menu.getComponent()) && isParentView(menu)) {
            component = UserConstant.PARENT_VIEW;
        }
        return component;
    }

    /**
     * 是否为菜单内部跳转
     * @param menu 菜单信息
     * @return
     */
    public boolean isMenuFrame(SysMenu menu) {
        return menu.getParentId().intValue() == 0
                && UserConstant.MENU_TYPE_MENU.equals(menu.getMenuType())
                && DictEnum.COMMON_YES_NO.NO.getValue().equals(menu.getIsFrame());
    }

    /**
     * 是否为内链组件
     * @param menu 菜单信息
     * @return
     */
    public boolean isInnerLink(SysMenu menu) {
        return DictEnum.COMMON_YES_NO.NO.getValue().equals(menu.getIsFrame()) && StrUtil.startWithAny(menu.getPath(), HttpConstant.HTTP, HttpConstant.HTTPS);
    }

    /**
     * 内链域名特殊字符替换
     * @param path
     * @return
     */
    public String innerLinkReplaceEach(String path) {
        String replace = StrUtil.replace(path, HttpConstant.HTTP, StrUtil.EMPTY);
        String replace1 = StrUtil.replace(replace, HttpConstant.HTTPS, StrUtil.EMPTY);
        String replace2 = StrUtil.replace(replace1, HttpConstant.WWW, StrUtil.EMPTY);
        return StrUtil.replace(replace2, ".", "/");
    }

    /**
     * 是否为parent_view组件
     * @param menu 菜单信息
     * @return
     */
    public boolean isParentView(SysMenu menu) {
        return menu.getParentId().intValue() != 0 && UserConstant.MENU_TYPE_DIR.equals(menu.getMenuType());
    }

}
