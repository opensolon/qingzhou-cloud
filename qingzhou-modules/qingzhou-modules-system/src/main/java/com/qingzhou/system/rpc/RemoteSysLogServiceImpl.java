package com.qingzhou.system.rpc;

import com.qingzhou.common.core.constants.ServiceConstant;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.system.api.RemoteSysLogService;
import com.qingzhou.system.api.domain.SysLoginLog;
import com.qingzhou.system.api.domain.SysOperLog;
import com.qingzhou.system.service.ISysLoginLogService;
import com.qingzhou.system.service.ISysOperLogService;
import org.noear.solon.annotation.Body;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Remoting;
import org.noear.solon.core.handle.Result;

/**
 * 远程调用系统服务实现 - 日志
 * @author xm
 */
@Remoting
@Mapping(ServiceConstant.PATH_TO_SYSTEM_LOG)
public class RemoteSysLogServiceImpl extends BaseController implements RemoteSysLogService {

    @Inject
    ISysLoginLogService sysLoginLogService;

    @Inject
    ISysOperLogService sysOperLogService;

    /**
     * 记录登录日志
     * @param sysLoginLog
     */
    @Override
    public Result<Void> saveLoginLog(@Body SysLoginLog sysLoginLog) {
        return toResult(sysLoginLogService.save(sysLoginLog));
    }

    /**
     * 记录操作日志
     * @param sysOperLog
     */
    @Override
    public Result<Void> saveOperLog(@Body SysOperLog sysOperLog) {
        return toResult(sysOperLogService.save(sysOperLog));
    }

}
