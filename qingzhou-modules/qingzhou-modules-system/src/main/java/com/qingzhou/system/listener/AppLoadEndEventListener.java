package com.qingzhou.system.listener;

import com.qingzhou.system.service.ISysConfigService;
import com.qingzhou.system.service.ISysDictDataService;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.event.AppLoadEndEvent;
import org.noear.solon.core.event.EventListener;

/**
 * 应用加载完成事件
 */
@Component
public class AppLoadEndEventListener implements EventListener<AppLoadEndEvent> {

    @Inject
    ISysDictDataService sysDictDataService;

    @Inject
    ISysConfigService sysConfigService;

    @Override
    public void onEvent(AppLoadEndEvent event) throws Throwable {
        // 缓存所有数据字典
        sysDictDataService.loadingDictCache();

        // 缓存系统参数
        sysConfigService.loadingConfigCache();
    }

}
