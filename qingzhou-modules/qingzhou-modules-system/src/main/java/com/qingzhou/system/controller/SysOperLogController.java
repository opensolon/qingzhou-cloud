package com.qingzhou.system.controller;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.qingzhou.common.core.enums.BusinessType;
import com.qingzhou.common.core.utils.ExcelUtil;
import com.qingzhou.common.datasource.annotation.QzLog;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.common.core.web.domain.PageData;
import com.qingzhou.system.api.domain.SysOperLog;
import com.qingzhou.system.service.ISysOperLogService;
import org.noear.solon.annotation.*;
import org.noear.solon.auth.annotation.AuthPermissions;
import org.noear.solon.core.handle.Result;

import java.util.Arrays;
import java.util.List;

/**
 * 操作日志 操作处理
 * @author xm
 */
@Controller
@Mapping("operlog")
public class SysOperLogController extends BaseController {

    @Inject
    ISysOperLogService sysOperLogService;

    /**
     * 查询操作日志列表
     */
    @AuthPermissions("system:operlog:list")
    @Get
    @Mapping("list")
    public PageData<SysOperLog> list(Page<SysOperLog> page, SysOperLog sysOperLog) {
        QueryWrapper qw = getQW(sysOperLog);
        Page<SysOperLog> result = sysOperLogService.page(page, qw);
        return getPageData(result);
    }

    /**
     * 导出操作日志列表
     */
    @QzLog(title = "操作日志", businessType = BusinessType.EXPORT)
    @AuthPermissions("system:operlog:export")
    @Post
    @Mapping("export")
    public void export(SysOperLog sysOperLog) {
        QueryWrapper qw = getQW(sysOperLog);
        List<SysOperLog> list = sysOperLogService.list(qw);
        ExcelUtil.export(list);
    }

    /**
     * 删除操作日志
     */
    @QzLog(title = "操作日志", businessType = BusinessType.DELETE)
    @AuthPermissions("system:operlog:remove")
    @Delete
    @Mapping("{operIds}")
    public Result<Void> remove(@Path String[] operIds) {
        return toResult(sysOperLogService.removeByIds(Arrays.asList(operIds)));
    }

    /**
     * 清空操作日志
     */
    @QzLog(title = "操作日志", businessType = BusinessType.CLEAN)
    @AuthPermissions("system:operlog:remove")
    @Delete
    @Mapping("clean")
    public Result<Void> clean() {
        sysOperLogService.clean();
        return Result.succeed();
    }

    private QueryWrapper getQW(SysOperLog sysOperLog) {
        QueryWrapper qw = QueryWrapper.create();
        qw.and(SysOperLog::getTitle).like(sysOperLog.getTitle());
        qw.and(SysOperLog::getBusinessType).eq(sysOperLog.getBusinessType());
        qw.and(SysOperLog::getStatus).eq(sysOperLog.getStatus());
        qw.and(SysOperLog::getOperName).like(sysOperLog.getOperName());
        qw.and(SysOperLog::getCreateTime).ge(sysOperLog.getStartTime());
        qw.and(SysOperLog::getCreateTime).le(sysOperLog.getEndTime());
        qw.orderBy(SysOperLog::getCreateTime).desc();
        return qw;
    }

}
