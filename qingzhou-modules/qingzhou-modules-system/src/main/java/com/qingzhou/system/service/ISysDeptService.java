package com.qingzhou.system.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.common.core.web.domain.entity.SysDept;
import com.qingzhou.system.domain.vo.TreeSelect;

import java.util.List;
import java.util.Map;

/**
 * 部门 服务层
 * @author xm
 */
public interface ISysDeptService extends IService<SysDept> {

    /**
     * 获取部门列表
     * @param sysDept
     * @return
     */
    List<SysDept> selectDeptList(SysDept sysDept);

    /**
     * 查询部门列表（排除节点）
     * @param deptId
     * @return
     */
    List<SysDept> excludeChild(Long deptId);

    /**
     * 查询部门树结构信息
     * @param sysDept 部门信息
     * @return
     */
    List<TreeSelect> selectDeptTreeList(SysDept sysDept);

    /**
     * 根据部门详情
     * @param deptId
     * @param sysDept
     * @return
     */
    SysDept info(Long deptId, SysDept sysDept);

    /**
     * 新增部门
     * @param sysDept
     * @return
     */
    int add(SysDept sysDept);

    /**
     * 修改部门
     * @param sysDept
     * @return
     */
    int edit(SysDept sysDept);

    /**
     * 删除部门
     * @param deptId
     * @param sysDept
     */
    int delete(Long deptId, SysDept sysDept);

    /**
     * 获取对应角色部门树列表
     * @param roleId
     * @param sysDept
     * @return
     */
    Map<String, Object> deptTree(Long roleId, SysDept sysDept);

}
