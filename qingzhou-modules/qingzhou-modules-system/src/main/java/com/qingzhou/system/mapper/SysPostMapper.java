package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.system.domain.SysPost;

import java.util.List;

/**
 * 岗位 数据层
 * @author xm
 */
public interface SysPostMapper extends BaseMapper<SysPost> {

    /**
     * 根据用户ID获取岗位选择框列表
     * @param userId 用户ID
     * @param delFlag
     * @return
     */
    List<Long> selectPostListByUserId(Long userId, int delFlag);

    /**
     * 查询用户所属岗位组
     * @param userName 用户名
     * @param delFlag
     * @return
     */
    List<SysPost> selectPostsByUserName(String userName, int delFlag);

}
