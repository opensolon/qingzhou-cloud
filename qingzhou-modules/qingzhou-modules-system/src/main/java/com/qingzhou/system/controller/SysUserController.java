package com.qingzhou.system.controller;

import com.mybatisflex.core.paginate.Page;
import com.qingzhou.common.core.constants.UserConstant;
import com.qingzhou.common.core.enums.BusinessType;
import com.qingzhou.common.core.utils.ExcelUtil;
import com.qingzhou.common.datasource.annotation.QzDataScope;
import com.qingzhou.common.datasource.annotation.QzLog;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.common.core.web.domain.PageData;
import com.qingzhou.common.core.web.domain.entity.SysRole;
import com.qingzhou.common.core.web.domain.entity.SysUser;
import com.qingzhou.system.service.ISysUserService;
import org.noear.solon.annotation.*;
import org.noear.solon.auth.annotation.AuthPermissions;
import org.noear.solon.core.handle.Result;
import org.noear.solon.data.annotation.Tran;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;
import java.util.Map;

/**
 * 系统用户 操作处理
 * @author xm
 */
@Controller
@Mapping("user")
public class SysUserController extends BaseController {

    @Inject
    ISysUserService sysUserService;

    /**
     * 查询用户列表
     */
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT, userAlias = UserConstant.DATA_SCOPE_ALIAS_USER)
    @AuthPermissions("system:user:list")
    @Get
    @Mapping("list")
    public PageData<SysUser> list(Page<SysUser> page, SysUser sysUser) {
        startPage(page);
        List<SysUser> list = sysUserService.selectUserList(sysUser);
        return getPageData(list);
    }

    /**
     * 导出用户列表
     */
    @QzLog(title = "系统用户", businessType = BusinessType.EXPORT)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT, userAlias = UserConstant.DATA_SCOPE_ALIAS_USER)
    @AuthPermissions("system:user:export")
    @Post
    @Mapping("export")
    public void export(SysUser sysUser) {
        List<SysUser> list = sysUserService.selectUserList(sysUser);
        ExcelUtil.export(list);
    }

    /**
     * 获取用户详情
     */
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT, userAlias = UserConstant.DATA_SCOPE_ALIAS_USER)
    @AuthPermissions("system:user:query")
    @Get
    @Mapping("{userId}")
    public Map<String, Object> info(@Path Long userId, SysUser sysUser, SysRole sysRole) {
        return sysUserService.info(userId, sysUser, sysRole);
    }

    /**
     * 获取用户详情
     */
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT, userAlias = UserConstant.DATA_SCOPE_ALIAS_USER)
    @AuthPermissions("system:user:query")
    @Get
    @Mapping
    public Map<String, Object> info(SysUser sysUser, SysRole sysRole) {
        return sysUserService.info(null, sysUser, sysRole);
    }

    /**
     * 新增用户
     */
    @QzLog(title = "系统用户", businessType = BusinessType.INSERT)
    @AuthPermissions("system:user:add")
    @Tran
    @Post
    @Mapping
    public Result<Void> add(@Body @Validated SysUser sysUser) {
        return toResult(sysUserService.add(sysUser));
    }

    /**
     * 修改用户
     */
    @QzLog(title = "系统用户", businessType = BusinessType.UPDATE)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT, userAlias = UserConstant.DATA_SCOPE_ALIAS_USER)
    @AuthPermissions("system:user:edit")
    @Tran
    @Put
    @Mapping
    public Result<Void> edit(@Body @Validated SysUser sysUser) {
        return toResult(sysUserService.edit(sysUser));
    }

    /**
     * 删除用户
     */
    @QzLog(title = "系统用户", businessType = BusinessType.DELETE)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT, userAlias = UserConstant.DATA_SCOPE_ALIAS_USER)
    @AuthPermissions("system:user:remove")
    @Tran
    @Delete
    @Mapping("{userIds}")
    public Result<Void> delete(@Path Long[] userIds, SysUser sysUser) {
        return toResult(sysUserService.delete(userIds, sysUser));
    }

    /**
     * 重置密码
     */
    @QzLog(title = "系统用户", businessType = BusinessType.UPDATE)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT, userAlias = UserConstant.DATA_SCOPE_ALIAS_USER)
    @AuthPermissions("system:user:edit")
    @Put
    @Mapping("resetPwd")
    public Result<Void> resetPwd(@Body SysUser sysUser) {
        return toResult(sysUserService.resetPwd(sysUser));
    }

    /**
     * 状态修改
     */
    @QzLog(title = "系统用户", businessType = BusinessType.UPDATE)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT, userAlias = UserConstant.DATA_SCOPE_ALIAS_USER)
    @AuthPermissions("system:user:edit")
    @Put
    @Mapping("changeStatus")
    public Result<Void> changeStatus(@Body SysUser sysUser) {
        return toResult(sysUserService.changeStatus(sysUser));
    }

    /**
     * 根据用户编号获取授权角色
     */
    @QzLog(title = "系统用户", businessType = BusinessType.GRANT)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT, userAlias = UserConstant.DATA_SCOPE_ALIAS_USER)
    @AuthPermissions("system:user:query")
    @Get
    @Mapping("authRole/{userId}")
    public Map<String, Object> authRole(@Path Long userId, SysRole sysRole) {
        return sysUserService.authRole(userId, sysRole);
    }

    /**
     * 用户授权角色
     */
    @QzLog(title = "系统用户", businessType = BusinessType.GRANT)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT, userAlias = UserConstant.DATA_SCOPE_ALIAS_USER)
    @AuthPermissions("system:user:edit")
    @Put
    @Mapping("authRole")
    public Result<Void> insertAuthRole(Long userId, Long[] roleIds, SysUser sysUser) {
        sysUserService.insertAuthRole(userId, roleIds, sysUser);
        return Result.succeed();
    }

    /**
     * 查询角色已授权用户列表
     */
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT, userAlias = UserConstant.DATA_SCOPE_ALIAS_USER)
    @AuthPermissions("system:role:list")
    @Get
    @Mapping("authUser/allocatedList")
    public PageData<SysUser> allocatedList(Page<SysUser> page, SysUser sysUser) {
        startPage(page);
        List<SysUser> list = sysUserService.selectAllocatedList(sysUser);
        return getPageData(list);
    }

    /**
     * 查询角色未授权用户列表
     */
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT, userAlias = UserConstant.DATA_SCOPE_ALIAS_USER)
    @AuthPermissions("system:role:list")
    @Get
    @Mapping("authUser/unallocatedList")
    public PageData<SysUser> unallocatedList(Page<SysUser> page, SysUser sysUser) {
        startPage(page);
        List<SysUser> list = sysUserService.selectUnallocatedList(sysUser);
        return getPageData(list);
    }

    /**
     * 个人中心 - 查询个人信息
     */
    @Get
    @Mapping("profile")
    public Map<String, Object> profile() {
        return sysUserService.profile();
    }

    /**
     * 个人中心 - 修改用户
     */
    @QzLog(title = "个人中心", businessType = BusinessType.UPDATE)
    @Put
    @Mapping("profile")
    public Result<Void> updateProfile(@Body SysUser sysUser) {
        return toResult(sysUserService.updateProfile(sysUser));
    }

    /**
     * 个人中心 - 修改密码
     */
    @QzLog(title = "个人中心", businessType = BusinessType.UPDATE)
    @Put
    @Mapping("profile/updatePwd")
    public Result<Void> updateProfilePwd(String oldPassword, String newPassword) {
        return toResult(sysUserService.updateProfilePwd(oldPassword, newPassword));
    }

    /**
     * 个人中心 - 修改头像
     */
    @QzLog(title = "个人中心", businessType = BusinessType.UPDATE)
    @Put
    @Mapping("profile/avatar")
    public Result<Void> updateProfileAvatar(String avatar) {
        return toResult(sysUserService.updateProfileAvatar(avatar));
    }

}
