package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.system.domain.SysConfig;

/**
 * 系统参数配置 数据层
 * @author xm
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
