package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.system.domain.SysNotice;

/**
 * 通知公告 数据层
 * @author xm
 */
public interface SysNoticeMapper extends BaseMapper<SysNotice> {

}
