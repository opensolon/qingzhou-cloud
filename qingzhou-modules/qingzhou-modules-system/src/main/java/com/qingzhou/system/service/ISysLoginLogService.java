package com.qingzhou.system.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.system.api.domain.SysLoginLog;

/**
 * 登录日志 服务层
 * @author xm
 */
public interface ISysLoginLogService extends IService<SysLoginLog> {

    /**
     * 解锁用户登录状态
     * @param userName
     * @return
     */
    void unlock(String userName);

    /**
     * 清空登录日志
     */
    void clean();

}
