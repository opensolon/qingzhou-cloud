package com.qingzhou.system.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.system.domain.SysDictData;

import java.util.List;

/**
 * 数据字典 服务层
 * @author xm
 */
public interface ISysDictDataService extends IService<SysDictData> {

    /**
     * 根据字典类型查询数据字典
     * @param dictType
     */
    List<SysDictData> getDataByType(String dictType);

    /**
     * 统计字典数据
     * @param dictType
     * @return
     */
    long countDictDataByType(String dictType);

    /**
     * 新增数据字典
     * @param sysDictData
     * @return
     */
    int add(SysDictData sysDictData);

    /**
     * 修改数据字典
     * @param sysDictData
     * @return
     */
    int edit(SysDictData sysDictData);

    /**
     * 修改字典类型
     * @param oldDictType
     * @param newDictType
     * @param cache 是否缓存
     */
    void updateDictDataType(String oldDictType, String newDictType, boolean cache);

    /**
     * 缓存数据字典
     * @param dictType
     */
    void cacheDictDataType(String dictType);

    /**
     * 删除数据字典
     * @param dictCodes
     */
    void delete(List<Long> dictCodes);

    /**
     * 缓存所有数据字典
     */
    void loadingDictCache();

}
