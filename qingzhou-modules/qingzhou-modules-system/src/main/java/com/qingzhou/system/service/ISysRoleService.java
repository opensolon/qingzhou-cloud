package com.qingzhou.system.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.common.core.web.domain.entity.SysRole;
import com.qingzhou.system.domain.SysUserRole;

import java.util.List;
import java.util.Set;

/**
 * 系统角色 服务层
 * @author xm
 */
public interface ISysRoleService extends IService<SysRole> {

    /**
     * 查询角色列表
     * @param sysRole
     * @return
     */
    List<SysRole> selectRoleList(SysRole sysRole);

    /**
     * 根据用户ID查询角色权限
     * @param userId
     * @return
     */
    Set<String> getRolePermission(long userId);

    /**
     * 根据用户ID查询角色列表
     * @param userId 用户ID
     * @param sysRole
     * @return
     */
    List<SysRole> selectRolesByUserId(Long userId, SysRole sysRole);

    /**
     * 根据用户名查询角色
     * @param userName 用户名
     * @return
     */
    List<SysRole> selectRolesByUserName(String userName);

    /**
     * 通过角色ID查询角色
     * @param roleId 角色ID
     * @return
     */
    SysRole selectRoleById(Long roleId);

    /**
     * 查询角色详情
     * @param roleId
     * @param sysRole
     * @return
     */
    SysRole info(Long roleId, SysRole sysRole);

    /**
     * 新增角色
     * @param sysRole
     * @return
     */
    boolean add(SysRole sysRole);

    /**
     * 修改角色
     * @param sysRole
     * @return
     */
    boolean edit(SysRole sysRole);

    /**
     * 修改保存数据权限
     * @param sysRole
     */
    boolean dataScope(SysRole sysRole);

    /**
     * 状态修改
     * @param sysRole
     * @return
     */
    int changeStatus(SysRole sysRole);

    /**
     * 删除角色
     * @param roleIds
     * @param sysRole
     */
    int delete(Long[] roleIds, SysRole sysRole);

    /**
     * 取消授权用户
     * @param sysUserRole
     * @return
     */
    int cancelAuthUser(SysUserRole sysUserRole);

    /**
     * 批量取消授权用户
     * @param roleId
     * @param userIds
     * @return
     */
    int cancelAuthUsers(Long roleId, Long[] userIds);

    /**
     * 批量选择用户授权
     * @param roleId
     * @param userIds
     * @param sysRole
     * @return
     */
    int insertAuthUsers(Long roleId, Long[] userIds, SysRole sysRole);

}
