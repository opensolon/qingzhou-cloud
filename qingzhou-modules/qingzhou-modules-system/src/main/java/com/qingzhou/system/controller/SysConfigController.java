package com.qingzhou.system.controller;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.qingzhou.common.core.enums.BusinessType;
import com.qingzhou.common.core.utils.ExcelUtil;
import com.qingzhou.common.datasource.annotation.QzLog;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.common.core.web.domain.PageData;
import com.qingzhou.system.domain.SysConfig;
import com.qingzhou.system.service.ISysConfigService;
import org.noear.solon.annotation.*;
import org.noear.solon.auth.annotation.AuthPermissions;
import org.noear.solon.core.handle.Result;
import org.noear.solon.validation.annotation.Validated;

import java.util.Arrays;
import java.util.List;

/**
 * 系统参数配置 操作处理
 * @author xm
 */
@Controller
@Mapping("config")
public class SysConfigController extends BaseController {

    @Inject
    ISysConfigService sysConfigService;

    /**
     * 查询系统参数配置列表
     */
    @AuthPermissions("system:config:list")
    @Get
    @Mapping("list")
    public PageData<SysConfig> list(Page<SysConfig> page, SysConfig sysConfig) {
        QueryWrapper qw = getQW(sysConfig);
        Page<SysConfig> result = sysConfigService.page(page, qw);
        return getPageData(result);
    }

    /**
     * 导出系统参数配置列表
     */
    @QzLog(title = "系统参数配置", businessType = BusinessType.EXPORT)
    @AuthPermissions("system:config:export")
    @Post
    @Mapping("export")
    public void export(SysConfig sysConfig) {
        QueryWrapper qw = getQW(sysConfig);
        List<SysConfig> list = sysConfigService.list(qw);
        ExcelUtil.export(list);
    }

    /**
     * 查询系统参数配置详情
     */
    @Get
    @Mapping("{id}")
    public SysConfig info(@Path Long id) {
        return sysConfigService.getById(id);
    }

    /**
     * 根据参数键名查询参数值
     */
    @Get
    @Mapping("configKey/{configKey}")
    public String getConfigKey(@Path String configKey) {
        return sysConfigService.selectConfigByKey(configKey);
    }

    /**
     * 新增系统参数配置
     */
    @QzLog(title = "系统参数配置", businessType = BusinessType.INSERT)
    @AuthPermissions("system:config:add")
    @Post
    @Mapping
    public Result<Void> add(@Body @Validated SysConfig sysConfig) {
        return toResult(sysConfigService.add(sysConfig));
    }

    /**
     * 修改系统参数配置
     */
    @QzLog(title = "系统参数配置", businessType = BusinessType.UPDATE)
    @AuthPermissions("system:config:edit")
    @Put
    @Mapping
    public Result<Void> edit(@Body @Validated SysConfig sysConfig) {
        return toResult(sysConfigService.edit(sysConfig));
    }

    /**
     * 删除系统参数配置
     */
    @QzLog(title = "系统参数配置", businessType = BusinessType.DELETE)
    @AuthPermissions("system:config:remove")
    @Delete
    @Mapping("{dictIds}")
    public Result<Void> delete(@Path Long[] dictIds) {
        sysConfigService.delete(Arrays.asList(dictIds));
        return Result.succeed();
    }

    /**
     * 刷新系统参数配置缓存
     */
    @QzLog(title = "系统参数配置", businessType = BusinessType.CLEAN)
    @AuthPermissions("system:config:remove")
    @Get
    @Mapping("refresh/cache")
    public Result<Void> refreshCache() {
        sysConfigService.loadingConfigCache();
        return Result.succeed();
    }

    private QueryWrapper getQW(SysConfig sysConfig) {
        QueryWrapper qw = QueryWrapper.create();
        qw.and(SysConfig::getConfigName).like(sysConfig.getConfigName());
        qw.and(SysConfig::getConfigType).eq(sysConfig.getConfigType());
        qw.and(SysConfig::getConfigKey).like(sysConfig.getConfigKey());
        return qw;
    }

}
