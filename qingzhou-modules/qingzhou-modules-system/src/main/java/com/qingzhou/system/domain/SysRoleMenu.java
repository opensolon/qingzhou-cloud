package com.qingzhou.system.domain;

import lombok.Data;

/**
 * 角色和菜单关联 对象
 * @author xm
 */
@Data
public class SysRoleMenu {

    /** 角色ID */
    private Long roleId;
    
    /** 菜单ID */
    private Long menuId;

}
