package com.qingzhou.system.rpc;

import com.qingzhou.common.core.constants.ServiceConstant;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.system.api.RemoteSysConfigService;
import com.qingzhou.system.service.ISysConfigService;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Remoting;
import org.noear.solon.core.handle.Result;

/**
 * 远程调用系统服务实现 - 系统参数
 * @author xm
 */
@Remoting
@Mapping(ServiceConstant.PATH_TO_SYSTEM_CONFIG)
public class RemoteSysConfigServiceImpl extends BaseController implements RemoteSysConfigService {

    @Inject
    ISysConfigService sysConfigService;

    /**
     * 根据参数键名查询参数值
     * @param configKey
     */
    @Override
    public Result<String> getSysConfigByKey(String configKey) {
        return Result.succeed(sysConfigService.selectConfigByKey(configKey));
    }

}
