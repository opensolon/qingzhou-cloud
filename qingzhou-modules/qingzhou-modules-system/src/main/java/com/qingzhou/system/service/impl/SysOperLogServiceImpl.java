package com.qingzhou.system.service.impl;

import com.mybatisflex.solon.service.impl.ServiceImpl;
import com.qingzhou.system.api.domain.SysOperLog;
import com.qingzhou.system.mapper.SysOperLogMapper;
import com.qingzhou.system.service.ISysOperLogService;
import org.apache.ibatis.solon.annotation.Db;
import org.noear.solon.annotation.Component;

/**
 * 操作日志 服务层实现
 * @author xm
 */
@Component
public class SysOperLogServiceImpl extends ServiceImpl<SysOperLogMapper, SysOperLog> implements ISysOperLogService {

    @Db
    SysOperLogMapper sysOperLogMapper;

    /**
     * 清空登录日志
     */
    @Override
    public void clean() {
        sysOperLogMapper.clean();
    }

}
