package com.qingzhou.system.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.system.domain.SysPost;

import java.util.List;

/**
 * 岗位 服务层
 * @author xm
 */
public interface ISysPostService extends IService<SysPost> {

    /**
     * 新增岗位
     * @param sysPost
     * @return
     */
    int add(SysPost sysPost);

    /**
     * 修改岗位
     * @param sysPost
     * @return
     */
    int edit(SysPost sysPost);

    /**
     * 删除岗位
     * @param postIds
     * @return
     */
    void delete(List<Long> postIds);

    /**
     * 根据用户ID获取岗位选择框列表
     * @param userId 用户ID
     * @return
     */
    List<Long> selectPostListByUserId(Long userId);

    /**
     * 查询用户所属岗位组
     * @param userName 用户名
     * @return
     */
    List<SysPost> selectPostsByUserName(String userName);

}
