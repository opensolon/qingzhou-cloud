package com.qingzhou.system.controller;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.qingzhou.common.core.enums.BusinessType;
import com.qingzhou.common.core.utils.ExcelUtil;
import com.qingzhou.common.datasource.annotation.QzLog;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.common.core.web.domain.PageData;
import com.qingzhou.system.domain.SysPost;
import com.qingzhou.system.service.ISysPostService;
import org.noear.solon.annotation.*;
import org.noear.solon.auth.annotation.AuthPermissions;
import org.noear.solon.core.handle.Result;
import org.noear.solon.validation.annotation.Validated;

import java.util.Arrays;
import java.util.List;

/**
 * 岗位 操作处理
 * @author xm
 */
@Controller
@Mapping("post")
public class SysPostController extends BaseController {

    @Inject
    ISysPostService sysPostService;

    /**
     * 查询岗位列表
     */
    @AuthPermissions("system:post:list")
    @Get
    @Mapping("list")
    public PageData<SysPost> list(Page<SysPost> page, SysPost sysPost) {
        QueryWrapper qw = getQW(sysPost);
        Page<SysPost> result = sysPostService.page(page, qw);
        return getPageData(result);
    }

    /**
     * 导出岗位列表
     */
    @QzLog(title = "岗位", businessType = BusinessType.EXPORT)
    @AuthPermissions("system:post:export")
    @Post
    @Mapping("export")
    public void export(SysPost sysPost) {
        QueryWrapper qw = getQW(sysPost);
        List<SysPost> list = sysPostService.list(qw);
        ExcelUtil.export(list);
    }

    /**
     * 查询岗位详情
     */
    @AuthPermissions("system:post:query")
    @Get
    @Mapping("{id}")
    public SysPost info(@Path Long id) {
        return sysPostService.getById(id);
    }

    /**
     * 新增岗位
     */
    @QzLog(title = "岗位", businessType = BusinessType.INSERT)
    @AuthPermissions("system:post:add")
    @Post
    @Mapping
    public Result<Void> add(@Body @Validated SysPost sysPost) {
        return toResult(sysPostService.add(sysPost));
    }

    /**
     * 修改岗位
     */
    @QzLog(title = "岗位", businessType = BusinessType.UPDATE)
    @AuthPermissions("system:post:edit")
    @Put
    @Mapping
    public Result<Void> edit(@Body @Validated SysPost sysPost) {
        return toResult(sysPostService.edit(sysPost));
    }

    /**
     * 删除岗位
     */
    @QzLog(title = "岗位", businessType = BusinessType.DELETE)
    @AuthPermissions("system:post:remove")
    @Delete
    @Mapping("{postIds}")
    public Result<Void> delete(@Path Long[] postIds) {
        sysPostService.delete(Arrays.asList(postIds));
        return Result.succeed();
    }

    /**
     * 获取岗位选择框列表
     */
    @Get
    @Mapping("optionselect")
    public List<SysPost> optionselect() {
        return sysPostService.list();
    }

    private QueryWrapper getQW(SysPost sysPost) {
        QueryWrapper qw = QueryWrapper.create();
        qw.and(SysPost::getPostCode).like(sysPost.getPostCode());
        qw.and(SysPost::getPostName).like(sysPost.getPostName());
        qw.and(SysPost::getStatus).eq(sysPost.getStatus());
        return qw;
    }

}
