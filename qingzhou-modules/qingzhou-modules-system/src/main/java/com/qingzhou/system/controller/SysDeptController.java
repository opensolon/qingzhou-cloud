package com.qingzhou.system.controller;

import com.qingzhou.common.core.constants.UserConstant;
import com.qingzhou.common.core.enums.BusinessType;
import com.qingzhou.common.datasource.annotation.QzDataScope;
import com.qingzhou.common.datasource.annotation.QzLog;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.common.core.web.domain.entity.SysDept;
import com.qingzhou.system.domain.vo.TreeSelect;
import com.qingzhou.system.service.ISysDeptService;
import org.noear.solon.annotation.*;
import org.noear.solon.auth.annotation.AuthPermissions;
import org.noear.solon.core.handle.Result;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;
import java.util.Map;

/**
 * 部门 操作处理
 * @author xm
 */
@Controller
@Mapping("dept")
public class SysDeptController extends BaseController {

    @Inject
    ISysDeptService sysDeptService;

    /**
     * 获取部门列表
     */
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:dept:list")
    @Get
    @Mapping("list")
    public List<SysDept> list(SysDept dept) {
        return sysDeptService.selectDeptList(dept);
    }

    /**
     * 查询部门列表（排除节点）
     */
    @AuthPermissions("system:dept:list")
    @Get
    @Mapping("list/exclude/{deptId}")
    public List<SysDept> excludeChild(@Path Long deptId) {
        return sysDeptService.excludeChild(deptId);
    }

    /**
     * 查询部门详情
     */
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:dept:query")
    @Get
    @Mapping("{deptId}")
    public SysDept info(@Path Long deptId, SysDept sysDept) {
        return sysDeptService.info(deptId, sysDept);
    }

    /**
     * 新增部门
     */
    @QzLog(title = "部门", businessType = BusinessType.INSERT)
    @AuthPermissions("system:dept:add")
    @Post
    @Mapping
    public Result<Void> add(@Body @Validated SysDept sysDept) {
        return toResult(sysDeptService.add(sysDept));
    }

    /**
     * 修改部门
     */
    @QzLog(title = "部门", businessType = BusinessType.UPDATE)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:dept:edit")
    @Put
    @Mapping
    public Result<Void> edit(@Body @Validated SysDept sysDept) {
        return toResult(sysDeptService.edit(sysDept));
    }

    /**
     * 删除部门
     */
    @QzLog(title = "部门", businessType = BusinessType.DELETE)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:dept:remove")
    @Delete
    @Mapping("{deptId}")
    public Result<Void> delete(@Path Long deptId, SysDept sysDept) {
        return toResult(sysDeptService.delete(deptId, sysDept));
    }

    /**
     * 获取对应角色部门树列表
     */
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:role:query")
    @Get
    @Mapping(value = "deptTree/{roleId}")
    public Result<Map<String, Object>> deptTree(@Path Long roleId, SysDept sysDept) {
        return Result.succeed(sysDeptService.deptTree(roleId, sysDept));
    }

    /**
     * 获取部门树列表
     */
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:user:list")
    @Get
    @Mapping("deptTree")
    public List<TreeSelect> deptTree(SysDept dept) {
        return sysDeptService.selectDeptTreeList(dept);
    }

}
