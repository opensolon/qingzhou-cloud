package com.qingzhou.system.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.system.domain.SysNotice;

/**
 * 通知公告 服务层
 * @author xm
 */
public interface ISysNoticeService extends IService<SysNotice> {

}
