package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.system.domain.SysDictData;

/**
 * 数据字典 数据层
 * @author xm
 */
public interface SysDictDataMapper extends BaseMapper<SysDictData> {

}
