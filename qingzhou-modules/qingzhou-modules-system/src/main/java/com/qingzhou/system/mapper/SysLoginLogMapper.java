package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.system.api.domain.SysLoginLog;

/**
 * 登录日志 数据层
 * @author xm
 */
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {

    /**
     * 清空登录日志
     */
    void clean();

}
