package com.qingzhou.system.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import com.qingzhou.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

/**
 * 岗位 对象
 * @author xm
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(value = "sys_post")
public class SysPost extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 岗位序号 */
    @Excel(name = "岗位序号")
    @Id
    private Long postId;

    /** 岗位编码 */
    @Excel(name = "岗位编码")
    @NotBlank(message = "岗位编码不能为空")
    @Length(max = 64, message = "岗位编码长度不能超过64个字符")
    private String postCode;

    /** 岗位名称 */
    @Excel(name = "岗位名称")
    @NotBlank(message = "岗位名称不能为空")
    @Length(max = 50, message = "岗位名称长度不能超过50个字符")
    private String postName;

    /** 岗位排序 */
    @Excel(name = "岗位排序")
    @NotNull(message = "显示顺序不能为空")
    private Integer postSort;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", replace = { "正常_0", "停用_1" })
    private String status;

    /** 用户是否存在此岗位标识 默认不存在 */
    @Column(ignore = true)
    private boolean flag = false;

}
