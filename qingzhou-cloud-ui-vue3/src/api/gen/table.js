import request from '@/utils/request'

// 查询数据库列表
export function listTables(query) {
  return request({
    url: '/gen/table/tables',
    method: 'get',
    params: query
  })
}

// 查询代码生成列表
export function listTable(query) {
  return request({
    url: '/gen/table/list',
    method: 'get',
    params: query
  })
}

// 查询代码生成详情
export function getTable(tableId) {
  return request({
    url: '/gen/table/' + tableId,
    method: 'get'
  })
}

// 预览生成代码
export function previewTable(tableId) {
  return request({
    url: '/gen/table/preview/' + tableId,
    method: 'get'
  })
}

// 新增代码生成（导入表）
export function addTable(data) {
  return request({
    url: '/gen/table',
    method: 'post',
    params: data
  })
}

// 修改代码生成信息
export function updateTable(data) {
  return request({
    url: '/gen/table',
    method: 'put',
    data: data
  })
}

// 删除代码生成
export function delTable(tableId) {
  return request({
    url: '/gen/table/' + tableId,
    method: 'delete'
  })
}

// 同步数据库
export function synchDb(tableName) {
  return request({
    url: '/gen/table/synchDb/' + tableName,
    method: 'get'
  })
}

// 生成代码（自定义路径）
export function genCode(tableName) {
  return request({
    url: '/gen/table/genCode/' + tableName,
    method: 'get'
  })
}
