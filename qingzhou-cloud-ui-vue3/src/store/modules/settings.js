import { useDynamicTitle } from '@/utils/dynamicTitle'

// 网页标题
export const title = import.meta.env.VITE_APP_TITLE

// 侧边栏主题 深色主题theme-dark，浅色主题theme-light
const sideTheme = 'theme-light'
// 是否系统布局配置
const showSettings = true
// 是否显示顶部导航
const topNav = false
// 是否显示 tagsView
const tagsView = true
// 是否固定头部
const fixedHeader = false
// 是否显示logo
const sidebarLogo = true
// 是否显示动态标题
const dynamicTitle = false

const storageSetting = JSON.parse(localStorage.getItem('layout-setting')) || ''

const useSettingsStore = defineStore(
  'settings',
  {
    state: () => ({
      title: '',
      theme: storageSetting.theme || '#409EFF',
      sideTheme: storageSetting.sideTheme || sideTheme,
      showSettings: showSettings,
      topNav: storageSetting.topNav === undefined ? topNav : storageSetting.topNav,
      tagsView: storageSetting.tagsView === undefined ? tagsView : storageSetting.tagsView,
      fixedHeader: storageSetting.fixedHeader === undefined ? fixedHeader : storageSetting.fixedHeader,
      sidebarLogo: storageSetting.sidebarLogo === undefined ? sidebarLogo : storageSetting.sidebarLogo,
      dynamicTitle: storageSetting.dynamicTitle === undefined ? dynamicTitle : storageSetting.dynamicTitle
    }),
    actions: {
      // 修改布局设置
      changeSetting(data) {
        const { key, value } = data
        if (Object.prototype.hasOwnProperty.call(this, key)) {
          this[key] = value
        }
      },
      // 设置网页标题
      setTitle(title) {
        this.title = title
        useDynamicTitle()
      }
    }
  })

export default useSettingsStore
