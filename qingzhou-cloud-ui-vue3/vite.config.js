import path from 'path'
import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import autoImport from 'unplugin-auto-import/vite'
import compression from 'vite-plugin-compression'
import eslint from 'vite-plugin-eslint'
import setupExtend from 'unplugin-vue-setup-extend-plus/vite'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'

// vite.config.js 配置说明（参考文档 https://vitejs.dev/config）
export default defineConfig(({ mode, command }) => {
  const env = loadEnv(mode, process.cwd())
  const isBuild = command === 'build'
  return {
    // 开发或生产环境服务的公共基础路径（如果应用被部署在一个子路径上，你就需要用这个选项指定这个子路径。例如，如果你的应用被部署在 https://www.my-app.com/my-app/，则设置 baseUrl 为 /my-app/）
    base: env.VITE_APP_CONTEXT_PATH,
    // 需要用到的插件数组
    plugins: [
      vue(),
      autoImport({
        dts: false,
        imports: ['vue', 'vue-router', 'pinia'],
        eslintrc: {
          enabled: true, // 启用 ESLint 自动导入配置
          filepath: './.eslintrc-auto-import.json', // 生成的文件路径
          globalsPropValue: true
        }
      }),
      compression(), // 启动gzip压缩
      eslint(),
      setupExtend({}),
      createSvgIconsPlugin({
        iconDirs: [path.resolve(process.cwd(), 'src/assets/icons/svg')],
        symbolId: 'icon-[dir]-[name]',
        svgoOptions: isBuild
      })
    ],
    resolve: {
      alias: {
        // 设置路径
        '~': path.resolve(__dirname, './'),
        // 设置别名
        '@': path.resolve(__dirname, './src')
      },
      // 导入时想要省略的扩展名列表
      extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue']
    },
    server: {
      host: '0.0.0.0',
      port: 80,
      open: true,
      proxy: {
        [env.VITE_APP_BASE_API + '/file']: {
          target: 'http://localhost:8091',
          changeOrigin: true,
          rewrite: (p) => p.replace(env.VITE_APP_BASE_API + '/file', '')
        },
        [env.VITE_APP_BASE_API + '/gen']: {
          target: 'http://localhost:8092',
          changeOrigin: true,
          rewrite: (p) => p.replace(env.VITE_APP_BASE_API + '/gen', '')
        },
        [env.VITE_APP_BASE_API + '/schedule']: {
          target: 'http://localhost:8093',
          changeOrigin: true,
          rewrite: (p) => p.replace(env.VITE_APP_BASE_API + '/schedule', '')
        },
        [env.VITE_APP_BASE_API + '/auth']: {
          target: 'http://localhost:8101',
          changeOrigin: true,
          rewrite: (p) => p.replace(env.VITE_APP_BASE_API + '/auth', '')
        },
        [env.VITE_APP_BASE_API + '/system']: {
          target: 'http://localhost:8102',
          changeOrigin: true,
          rewrite: (p) => p.replace(env.VITE_APP_BASE_API + '/system', '')
        },
        [env.VITE_APP_BASE_API + '/demo']: {
          target: 'http://localhost:8103',
          changeOrigin: true,
          rewrite: (p) => p.replace(env.VITE_APP_BASE_API + '/demo', '')
        }
      }
    },
    // fix:error:stdin>:7356:1: warning: "@charset" must be the first rule in the file
    css: {
      postcss: {
        plugins: [
          {
            postcssPlugin: 'internal:charset-removal',
            AtRule: {
              charset: (atRule) => {
                if (atRule.name === 'charset') {
                  atRule.remove()
                }
              }
            }
          }
        ]
      }
    }
  }
})
